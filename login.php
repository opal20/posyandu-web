<?php

session_start();
require_once "koneksi.php";
$conn  = koneksi();
$error = '';

if(isset($_POST['submit']))
{
    $username = $_POST['username'];
    $password = $_POST['password'];

    $query = mysqli_query($conn, "SELECT * FROM user WHERE username='$username'");

    if(mysqli_num_rows($query) == 0)
    {
        $error = "Username salah";
    }
    else
    {
        $row = mysqli_fetch_assoc($query);
      if(password_verify($password, $row["password"]))
				{
        
        $_SESSION['username'] = $row['username'];
        $_SESSION['level']    = $row['level'];
        $_SESSION['id']    = $row['id_user'];


            header("Location: admin/");


      }else
      {
          $error = "Pasword Salah";
      }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Form Login</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="_assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="_assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="_assets/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="_assets/dist/css/AdminLTE.min.css">

  <!-- Pace Loader -->
  <link rel="stylesheet" href="_assets/pace/pace-blue-minimal.css" />
  <script src="_assets/pace/pace.min.js"></script>

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Form</b> Login
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

    <p class="login-box-msg">Masukkan username dan password Anda</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="Username" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-2">
          <div class="checkbox icheck">
            <label>
              <!-- text -->
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-8">
          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
        </div>

        <!-- /.col -->
      </div>

      <div style="Color:#ff0808 !important; text-align:center !important;"class="has-error">
        <?php echo $error; ?>
      </div>
    </form>

  </div> <!-- /.login-box-body -->
</div> <!-- /.login-box -->

<script src="_assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
