-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Nov 2020 pada 09.03
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_posyandu`
--
CREATE DATABASE IF NOT EXISTS `db_posyandu` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `db_posyandu`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `balita`
--

DROP TABLE IF EXISTS `balita`;
CREATE TABLE `balita` (
  `id_balita` int(9) NOT NULL,
  `nomor_kk` int(20) DEFAULT NULL,
  `id_user` int(3) NOT NULL,
  `nama_balita` varchar(100) DEFAULT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `anak_ke` int(3) NOT NULL,
  `berat_lahir` float NOT NULL,
  `panjang_lahir` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_balita`
--

DROP TABLE IF EXISTS `detail_balita`;
CREATE TABLE `detail_balita` (
  `id_detail` int(9) NOT NULL,
  `id_balita` int(9) NOT NULL,
  `usia` int(2) DEFAULT NULL,
  `berat_badan` float DEFAULT NULL,
  `status_gizi` enum('1','2','3','4') DEFAULT NULL,
  `imunisasi` varchar(255) DEFAULT NULL,
  `vitamin` varchar(255) DEFAULT NULL,
  `obat` varchar(255) DEFAULT NULL,
  `tgl_update` date NOT NULL,
  `nomor_urut` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `orang_tua`
--

DROP TABLE IF EXISTS `orang_tua`;
CREATE TABLE `orang_tua` (
  `nomor_kk` int(20) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL,
  `nama_ayah` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(5) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `no_tlp` varchar(25) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `level` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `no_tlp`, `email`, `level`) VALUES
(13, 'kader1-romzah', '$2y$10$KHhdO4Chyw8FtmNteLHC2uBXaeI4WsAmGZAGC7ABuPidXJRWqyLYK', '0812', 'romyheart16@gmail.com', '1'),
(14, 'bidan1-maharani', '$2y$10$DQYRXr7Jnrba6iFE8pvQouCo2rqqTCz71ai4qRauiKOwYJC.TKIOG', '089998889', 'romyheart16@gmail.com12', '2');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `balita`
--
ALTER TABLE `balita`
  ADD PRIMARY KEY (`id_balita`),
  ADD KEY `id_ortu` (`nomor_kk`);

--
-- Indeks untuk tabel `detail_balita`
--
ALTER TABLE `detail_balita`
  ADD PRIMARY KEY (`id_detail`),
  ADD KEY `id_balita` (`id_balita`);

--
-- Indeks untuk tabel `orang_tua`
--
ALTER TABLE `orang_tua`
  ADD PRIMARY KEY (`nomor_kk`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `balita`
--
ALTER TABLE `balita`
  MODIFY `id_balita` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `detail_balita`
--
ALTER TABLE `detail_balita`
  MODIFY `id_detail` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `orang_tua`
--
ALTER TABLE `orang_tua`
  MODIFY `nomor_kk` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123133;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `balita`
--
ALTER TABLE `balita`
  ADD CONSTRAINT `balita_ibfk_1` FOREIGN KEY (`nomor_kk`) REFERENCES `orang_tua` (`nomor_kk`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `detail_balita`
--
ALTER TABLE `detail_balita`
  ADD CONSTRAINT `detail_balita_ibfk_1` FOREIGN KEY (`id_balita`) REFERENCES `balita` (`id_balita`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
