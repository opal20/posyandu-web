<?php
  require_once 'koneksi.php';
  $conn   = koneksi();
  $hasil  = mysqli_query($conn,"select count(status_gizi) as jumlah from detail_balita where status_gizi='1'");
  $datas  = mysqli_fetch_array($hasil);
  $array  = array(65,90,75,81,95,105,130);
  $jumlah = json_encode($array);
  $array_one  = array(55,90,55,81,95,55,130);
  $jumlah_one = json_encode($array_one);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Selamat Datang</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="_assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="_assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="_assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="_assets/bower_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="_assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="_assets/dist/css/skins/_all-skins.min.css">

  <!-- Pace Loader -->
  <link rel="stylesheet" href="_assets/pace/pace-white-minimal.css" />
  <script src="_assets/pace/pace.min.js"></script>

</head>

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">
    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <a href="./" class="navbar-brand"><b>Selamat</b> Datang</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="login.php" title="Masuk">Masuk <i class="fa fa-sign-in"></i></a></li>
            </ul>
          </div> <!-- /.navbar-collapse -->
        </div> <!-- /.container-fluid -->
      </nav>
    </header>

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Dashboard
            <small>Halaman Utama</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Selamat Datang</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-child"></i></span>
                <?php
                  $conn  = koneksi();
                  $hasil = mysqli_query($conn,"select count(status_gizi) as jumlah from detail_balita where status_gizi='1'");
                  $lebih = mysqli_fetch_array($hasil);
                ?>
                <div class="info-box-content">
                  <span class="info-box-text">Gizi Lebih</span>
                  <span class="info-box-number" style="font-size: 30px;"><?=$lebih['jumlah']?></span>
                </div> <!-- /.info-box-content -->
              </div> <!-- /.info-box -->
            </div> <!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-child"></i></span>
                <?php
                  $conn  = koneksi();
                  $hasil = mysqli_query($conn,"select count(status_gizi) as jumlah from detail_balita where status_gizi='2'");
                  $baik = mysqli_fetch_array($hasil);
                ?>
                <div class="info-box-content">
                  <span class="info-box-text">Gizi Baik</span>
                  <span class="info-box-number" style="font-size: 30px;"><?=$baik['jumlah']?></span>
                </div> <!-- /.info-box-content -->
              </div> <!-- /.info-box -->
            </div> <!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-child"></i></span>
                <?php
                  $conn  = koneksi();
                  $hasil = mysqli_query($conn,"select count(status_gizi) as jumlah from detail_balita where status_gizi='3'");
                  $kurang = mysqli_fetch_array($hasil);
                ?>
                <div class="info-box-content">
                  <span class="info-box-text">Gizi Kurang</span>
                  <span class="info-box-number" style="font-size: 30px;"><?=$kurang['jumlah']?></span>
                </div> <!-- /.info-box-content -->
              </div> <!-- /.info-box -->
            </div> <!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-child"></i></span>
                <?php
                  $conn  = koneksi();
                  $hasil = mysqli_query($conn,"select count(status_gizi) as jumlah from detail_balita where status_gizi='4'");
                  $buruk = mysqli_fetch_array($hasil);
                ?>
                <div class="info-box-content">
                  <span class="info-box-text">Gizi Buruk</span>
                  <span class="info-box-number" style="font-size: 30px;"><?=$buruk['jumlah']?></span>
                </div> <!-- /.info-box-content -->
              </div> <!-- /.info-box -->
            </div> <!-- /.col -->

          </div> <!-- /.row -->



        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

    <?php include('admin/footer.php'); ?>
</body>
</html>
