<?php
function tgl_indo_jam($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$space = explode(' ', $tanggal);
  $jam = explode(':',$space[1]);
	$pecahkan = explode('-', $space[0]);
	// var_dump($jam) ;
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
?>
