<?php include_once('header.php'); ?>

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Dashboard
            <small>Halaman Utama</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Selamat Datang</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-child"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Gizi Lebih</span>
                  <span class="info-box-number">1,410</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-child"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Gizi Baik</span>
                  <span class="info-box-number">410</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-child"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Gizi Kurang</span>
                  <span class="info-box-number">13,648</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-child"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Gizi Buruk</span>
                  <span class="info-box-number">93,139</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
         
        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

<?php include_once('footer.php'); ?>