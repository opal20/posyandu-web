  <footer class="main-footer">
      <div class="container">
        <div class="pull-right hidden-xs">
          version 1.0.0 | Developed by <a href="" title="Developer">Abdul Hafizh</a>
        </div>
        <strong>Copyright &copy; 2018 <a href="https://adminlte.io" target="_blank">Almsaeed Studio</a>.</strong>
      </div> <!-- /.container -->
    </footer>
  </div> <!-- ./wrapper -->

  <script src="../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../_assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../_assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="../_assets/bower_components/select2/dist/js/select2.full.min.js"></script>
  <script src="../_assets/dist/js/adminlte.min.js"></script>
</body>
</html>