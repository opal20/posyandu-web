<?php 
  require_once '../koneksi.php'; 

  session_start();
  if(!isset($_SESSION['username'])){    
      header('Location:../login.php');
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Selamat Datang</title>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../_assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../_assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../_assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="../_assets/bower_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="../_assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../_assets/dist/css/skins/_all-skins.min.css">

  <!-- Pace Loader -->
  <link rel="stylesheet" href="../_assets/pace/pace-white-minimal.css" />
  <script src="../_assets/pace/pace.min.js"></script>
  
</head>

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">
    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <a href="./" class="navbar-brand"><b>Selamat</b> Datang</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">              
              <li><a href="./">Beranda</a></li>
              <li><a href="profile/">Profil</a></li>              
              <li><a href="balita/">Balita</a></li>              
              <li><a href="prediksi/">Prediksi</a></li>                            
              <li><a href="../logout.php" title="Keluar">Keluar <i class="fa fa-sign-out"></i></a></li>
            </ul>
          </div> <!-- /.navbar-collapse -->
        </div> <!-- /.container-fluid -->
      </nav>
    </header>