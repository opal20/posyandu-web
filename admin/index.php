<?php include_once('header.php');
?>

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Dashboard
            <small>Halaman Utama</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Selamat Datang </li> <?php echo $_SESSION['username'] ?>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-child"></i></span>
                <?php
                  $conn  = koneksi();
              //  var_dump($_SESSION['level']);
                  $hasil = mysqli_query($conn,"select count(status_gizi) as jumlah from detail_balita where status_gizi='1'");
                  $lebih = mysqli_fetch_array($hasil);
                ?>
                <div class="info-box-content">
                  <span class="info-box-text">Gizi Lebih</span>
                  <span class="info-box-number" style="font-size: 30px;"><?=$lebih['jumlah']?></span>
                </div> <!-- /.info-box-content -->
              </div> <!-- /.info-box -->
            </div> <!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-child"></i></span>
                <?php
                  $conn  = koneksi();
                  $hasil = mysqli_query($conn,"select count(status_gizi) as jumlah from detail_balita where status_gizi='2'");
                  $baik = mysqli_fetch_array($hasil);
                ?>
                <div class="info-box-content">
                  <span class="info-box-text">Gizi Baik</span>
                  <span class="info-box-number" style="font-size: 30px;"><?=$baik['jumlah']?></span>
                </div> <!-- /.info-box-content -->
              </div> <!-- /.info-box -->
            </div> <!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-child"></i></span>
                <?php
                  $conn  = koneksi();
                  $hasil = mysqli_query($conn,"select count(status_gizi) as jumlah from detail_balita where status_gizi='3'");
                  $kurang = mysqli_fetch_array($hasil);
                ?>
                <div class="info-box-content">
                  <span class="info-box-text">Gizi Kurang</span>
                  <span class="info-box-number" style="font-size: 30px;"><?=$kurang['jumlah']?></span>
                </div> <!-- /.info-box-content -->
              </div> <!-- /.info-box -->
            </div> <!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-child"></i></span>
                <?php
                  $conn  = koneksi();
                  $hasil = mysqli_query($conn,"select count(status_gizi) as jumlah from detail_balita where status_gizi='4'");
                  $buruk = mysqli_fetch_array($hasil);
                ?>
                <div class="info-box-content">
                  <span class="info-box-text">Gizi Buruk</span>
                  <span class="info-box-number" style="font-size: 30px;"><?=$buruk['jumlah']?></span>
                </div> <!-- /.info-box-content -->
              </div> <!-- /.info-box -->
            </div> <!-- /.col -->

          </div> <!-- /.row -->

        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

<?php include_once('footer.php'); ?>
