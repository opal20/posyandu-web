<?php
  require_once '../../koneksi.php';
  $id    = $_GET['id'];
  if(isset($_POST['update'])){
    $id    = $_GET['id'];
    $nama   = $_POST['nama_balita'];
    $gender = $_POST['jenis_kelamin'];
    $tgl    = $_POST['tgl_lahir'];
    $anakke = $_POST['anak_ke'];
    $berat_lahir   = $_POST['berat'];
    $panjang_lahir   = $_POST['panjang'];
    $conn  = koneksi();
    // var_dump($nama);
    // var_dump($gender);
    // var_dump($tgl);
    // var_dump($berat_lahir);
    // var_dump($panjang_lahir);
    // var_dump($anakke);
    // var_dump($id);
    // exit;
    $hasils  = mysqli_query($conn, "update balita set
      nama_balita='$nama',
      jenis_kelamin='$gender',
      tgl_lahir='$tgl',
      anak_ke='$anakke',
      berat_lahir='$berat_lahir',
      panjang_lahir='$panjang_lahir'


       where id_balita='$id'");

    if(!$hasils) {
      echo "<script>alert('Gagal Ubah Data')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
    else {
      echo "<script>alert('Data Berhasil Diubah')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
  }
?>

<!DOCTYPE html>
<html>
<?php include_once('../../header.php'); ?>

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Orang Tua
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Orang Tua</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-8">
              <div class="box box-info">
                <?php
                $id    = $_GET['id'];
                  $conn  = koneksi();
                  $hasil = mysqli_query($conn,"select * from balita inner join orang_tua as ortu on ortu.nomor_kk = balita.nomor_kk where id_balita='$id'");
                  $data  = mysqli_fetch_array($hasil);
                ?>
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Data Bayi Nyonya <?=$data['nama_ibu']?></h3>
                </div> <!-- /.box-header -->

                <div class="box-body">
                  <!-- form start -->

                  <form class="form-horizontal" method="POST">
                    <input type="hidden" name="nomor_kk" value="<?=$data['nomor_kk']?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Nama Balita</label>
                        <div class="col-sm-7">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-child"></i>
                            </div>
                            <input type="text" class="form-control" name="nama_balita" placeholder="Nama Balita" value="<?=$data['nama_balita']?>"required>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Jenis Kelamin</label>
                        <div class="col-sm-7">
                          <select class="form-control select2" name="jenis_kelamin" required>
                            <option value="" readonly >Pilih Jenis Kelamin</option>
                            <option value="L" <?php if($data['jenis_kelamin']=="L") echo "selected" ?>>Laki-laki</option>
                            <option value="P" <?php if($data['jenis_kelamin']=="P") echo "selected" ?>>Perempuan</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Tanggal Lahir</label>
                        <div class="col-sm-7">
                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal Lahir"  value="<?=$data['tgl_lahir']?>" required>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Anak ke -</label>
                        <div class="col-sm-7">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-child"></i>
                            </div>
                            <input type="number" min="0" max="50" step="any" class="form-control" name="anak_ke" placeholder="Anak ke" value="<?=$data['anak_ke']?>"  required>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Berat Lahir</label>
                        <div class="col-sm-7">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-child"></i>
                            </div>
                            <input type="number" min="0" max="50" step="any" class="form-control" name="berat" placeholder="Berat Lahir" value="<?=$data['berat_lahir']?>" required>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Panjang Lahir</label>
                        <div class="col-sm-7">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-child"></i>
                            </div>
                            <input type="number" min="0" max="70" step="any" class="form-control" name="panjang" placeholder="Nama Balita" value="<?=$data['panjang_lahir']?>" required>
                          </div>
                        </div>
                      </div>
                    </div> <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" name="update" class="btn btn-warning pull-right"><i class="fa fa-save"></i> Simpan</button>
                    </div> <!-- /.box-footer -->
                  </form>
                </div> <!-- /.box-body -->
              </div> <!-- /.box -->
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->

        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

      <?php include_once('../footer.php'); ?>
  </div> <!-- ./wrapper -->


</body>
</html>
<script>
$(function () {

  $('#datepicker').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayHighlight:true
  })


})
</script>
