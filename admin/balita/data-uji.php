<?php 
  require_once '../../koneksi.php';
  require_once '../../_assets/_fungsiTanggal.php';

  session_start();
  if(!isset($_SESSION['username'])){    
      header('Location:../../login.php');
  }  
?>

<?php include_once('header.php'); ?>

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Data Balita
            <small>Data Uji (Testing)</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="./"><i class="fa fa-child"></i> Balita</a></li>
            <li class="active">Data Uji (Testing)</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">

            <div class="col-md-">
              <div class="box b12ox-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Uji (Testing)</h3>
                </div> <!-- /.box-header -->

                <div class="box-body">
                  <table id="balita" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Posyandu</th>
                        <th>Nama Balita</th>
                        <th>Nama Ibu</th>
                        <th>Jenis Kelamin</th>
                        <th>Tanggal Lahir</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php                        
                        $conn = koneksi();                   
                        $sql =  "select * from balita
                                  left join posyandu on balita.id_posyandu=posyandu.id_posyandu
                                  left join orang_tua on balita.nomor_kk=orang_tua.nomor_kk
                                  where balita.jenis_data = '1'";
                                  
                        $hasil = mysqli_query($conn, $sql);                      
                        $no    = 1;
                        while ($data = mysqli_fetch_array($hasil)) {                        
                          $gender="";
                          if($data["jenis_kelamin"]=="L"){
                              $gender="Laki-laki";
                          }
                          if($data["jenis_kelamin"]=="P"){
                              $gender="Perempuan";
                          }
                      ?>
                      <tr>
                        <td><?=$no++?></td>
                        <td><?=$data["nama_posyandu"]?></td>
                        <td><?=$data["nama_balita"]?></td>
                        <td><?=$data["nama_ibu"]?></td>
                        <td><?=$gender?></td>
                        <td><?=tgl_indo($data["tgl_lahir"])?></td>
                        <td>
                          <a href="detail.php?id=<?=$data['id_balita'];?>" class="btn btn-sm btn-primary" title="Detail Data"><i class="fa fa-search"></i></a>                          
                        </td>
                      </tr>
                      <?php } ?>

                    </tbody>
                  </table>
                </div> <!-- /.box-body -->
               
              </div> <!-- /.box -->          
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->
         
        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="container">
        <div class="pull-right hidden-xs">
          version 1.0.0 | Developed by <a href="" title="Developer">Abdul Hafizh</a>
        </div>
        <strong>Copyright &copy; 2018 <a href="https://adminlte.io" target="_blank">Almsaeed Studio</a>.</strong>
      </div> <!-- /.container -->
    </footer>
  </div> <!-- ./wrapper -->

  <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../_assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../../_assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="../../_assets/dist/js/adminlte.min.js"></script>
  <script>
    $(function () {
      $('#balita').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'info'        : true,
        'autoWidth'   : true,
        'columnDefs': [
          { 
            "targets": [ 4 ], 
            "orderable": false
          },
          { 
            "targets": [ 5,6 ], 
            "searchable": false,
            "orderable": false
          },
          { 
            "targets": [ 0 ], 
            "searchable": false
          },
        ]
      })
    })
  </script>
</body>
</html>