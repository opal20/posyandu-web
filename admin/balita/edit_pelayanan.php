<?php include_once('../../header.php'); ?>

<?php
  require_once '../../koneksi.php';
  $id    = $_GET['id'];
  $id_balita = $_GET['balita'];
  $id_user = $_SESSION['id'];
  $conn  = koneksi();



    $hasil = mysqli_query($conn,"select * from detail_balita
              where id_detail='$id'");
    $data  = mysqli_fetch_array($hasil);
    $status_gizi = $data['status_gizi'];
  if(isset($_POST['simpan'])){


    if(!empty($_POST['imunisasi']) || !empty($_POST['vitamin']) || !empty($_POST['obat'])){
      $usia   = $_POST['usia'];
      $berat  = $_POST['berat_badan'];
      // $status = $_POST['status_gizi'];
      $tgl    = date('Y-m-d');
      $imunisasi = "";
      $vitamin = "";
      $obat = "";
      if(isset($_POST['imunisasi'])){
        $imunisasi = $_POST['imunisasi'];
      }else{
        $imunisasi = null;
      }
      if(isset($_POST['vitamin'])){
        $vitamin = $_POST['vitamin'];
      }else{
        $vitamin = null;
      }
      if($_POST['obat']=""){
        $obat = $_POST['obat'];
      }else{
        $obat = null;
      }


      $input_imunisasi = implode(", ", $imunisasi);
    //   var_dump($usia);
    //   var_dump($berat);
    //   var_dump($status_gizi);
    //   var_dump($input_imunisasi);
    //
    //   var_dump($vitamin);
    //   echo "<br>";
    //     echo $obat;
    // exit;

      $sql   = "update detail_balita set id_balita='$id_balita',id_user='$id_user', usia='$usia',berat_badan='$berat',
                            status_gizi='$status_gizi',imunisasi='$input_imunisasi', vitamin='$vitamin', obat='$obat', tgl_update='$tgl' where id_detail='$id'";

    }else{
      $sql   = "update detail_balita set id_balita='$id_balita',id_user='$id_user',usia='$usia',berat_badan='$berat',
                            status_gizi='$status' where id_detail='$id'";
    }
    $hasil  = mysqli_query($conn, $sql);
    if(!$hasil) {
      echo "<script>alert('Gagal Ubah Data')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
    else {
      echo "<script>alert('Data Berhasil Diubah')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
  }
?>

<!DOCTYPE html>
<html>

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Orang Tua
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Orang Tua</li>
          </ol>
        </section>
        <?php
        $balita_data = mysqli_query($conn,"select * from balita
                  where id_balita='$id_balita'");
        $data_bayi  = mysqli_fetch_array($balita_data);

         ?>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-8">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Data Pelayanan bayi <strong><?=$data_bayi["nama_balita"]?></strong></h3>
                  <h4><span class="small">Pada pemeriksaan ke <?=$data['nomor_urut']?></span></h4>
                </div> <!-- /.box-header -->

                <div class="box-body">
                  <!-- form start -->

                  <form class="form-horizontal"method="POST">

                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Usia (Bulan)</label>
                        <div class="col-sm-7">
                          <input type="number" min="0"id="usia-bayi" class="form-control" name="usia" placeholder="Usia Balita"  value="<?=$data['usia']?>" required readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Berat (Kg)</label>
                        <div class="col-sm-7">
                          <input type="number" id="beratBadan"min="0" max="50" step="any" class="form-control" name="berat_badan" placeholder="Berat Balita" value="<?=$data['berat_badan']?>" required readonly>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Status Gizi</label>

                        <div class="col-sm-7">
                          <select name="status_gizi" class="form-control" required readonly>
                            <?php
                            $array_gizi = array(
                              '1'=>'Gizi Lebih',
                              '2' => 'Gizi Baik',
                              '3'=>'Gizi Kurang',
                              '4' => 'Gizi Buruk',


                            );


                            foreach ($array_gizi as $key => $value) {
                                if(in_array($status_gizi,array($key))){

                                  echo "<option value='".$key."' selected>".$value."</option>";
                                }

                              echo "<option value='".$key."'>".$value."</option>";
                            }
                             ?>

                          </select>
                        </div>
                      </div>

                      <?php
                      $disabled = "";
                      $text = "Hanya Bidan yang dapat mengakses Imunisasi, Vitamin, Obat";
                      if($kader){
                        $disabled = "disabled";
                        ?> <span class="small"><?=$text?></span><br><?php
                      }
                       ?>




                      <div class="form-group">
                        <label class="col-sm-4 control-label">Imunisasi</label>
                        <div class="col-sm-7">
                          <?php
                          $array_imunisasi = array(
                            'HB-0', 'BCG+Polio-1', 'DPT/HB1+Polio-2', 'DPT/HB2+Polio-3', 'DPT/HB3+Polio-4', 'Campak'
                          );
                          // $imunisasi = $data['imunisasi'];
                          // $array_data = $imunisasi;
                          $array_data=explode(', ', $data['imunisasi']);
                          // var_dump($array_data);
                          foreach($array_imunisasi as $value){
                            $checked = "";
                          if(in_array($value,$array_data)){
                            $checked = "checked";
                          }
                              //var_dump($check);



                            echo '<input type="checkbox" value='.$value.' name="imunisasi[]" '.$disabled.' '.$checked.'/> '. $value.'<br>' ;
                          }
                           ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Vitamin A</label>
                        <div class="col-sm-7">
                        <input type="radio" name="vitamin" value="Kapsul Merah" <?php if($data['vitamin']=="Kapsul Merah") echo "checked" ?>> Kapsul Merah<br>
                      <input type="radio"  name="vitamin" value="Kapsul Biru" <?php if($data['vitamin']=="Kapsul Biru") echo "checked" ?>> Kapsul Biru
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Obat</label>
                        <div class="col-sm-7">
                          <div class="input-group">

                            <input type="text" class="form-control" name="obat" placeholder="obat" <?=$disabled?>>
                          </div>
                        </div>
                      </div>
                    </div> <!-- /.box-body -->

                    <div class="box-footer">
                      <a href="../balita/detail.php?id=<?=$id?>" class="btn btn-dark btn-sm" title="Kembali Ke Halaman Semua Balita">
                        <i class="fa fa-arrow-left"></i> Kembali</a>
                      <input type="submit" name="simpan" class="btn btn-success pull-right"/>
                    </div> <!-- /.box-footer -->
                  </form>
                </div> <!-- /.box-body -->
              </div> <!-- /.box -->
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->

        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

      <?php include_once('../footer.php'); ?>
  </div> <!-- ./wrapper -->

  <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../_assets/dist/js/adminlte.min.js"></script>
</body>
</html>
