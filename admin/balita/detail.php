<?php
  require_once '../../koneksi.php';

  require_once '../../_assets/_fungsiTanggal.php';
  $id    = $_GET['id'];
  $conn  = koneksi();
  $hasil = mysqli_query($conn,"select usia,berat_badan from detail_balita
            inner join balita on detail_balita.id_balita = balita.id_balita
            where balita.id_balita='$id'");
  while ($data = mysqli_fetch_array($hasil)) {
    $chartusia[]  = $data["usia"];
    $chartberat[] = $data["berat_badan"];
  }
?>

<?php
include('../../header.php');

$cek =  $_SESSION['level'];
$id_user = $_SESSION['id'];
$admin = false;
$kader = false;
$bidan = false;
$ortu = false;
if ($cek == 0){
    $admin = true;
}elseif ($cek == 1){

  $kader = true;
}elseif ($cek == 2) {
  // code...

  $bidan = true;

}elseif ($cek == 3) {
// code...

$ortu = true;
}
 ?>

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Detail Balita
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="./"><i class="fa fa-child"></i> Semua Balita</a></li>
            <li class="active">Detail Balita</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-4 hapus">
            <!-- Horizontal Form -->
              <?php
                  $id    = $_GET['id'];
                  $conn  = koneksi();
                  $hasil = mysqli_query($conn,"select * from detail_balita
                            inner join balita on detail_balita.id_balita = balita.id_balita
                            inner join orang_tua on balita.nomor_kk = orang_tua.nomor_kk
                            where balita.id_balita='$id'");
                  $data  = mysqli_fetch_array($hasil);
                  // var_dump($_SESSION);


              ?>
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Balita</h3>
                </div> <!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nomor KK</label>
                      <div class="col-sm-7">
                        : <?=$data["nomor_kk"]?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nama Ibu</label>
                      <div class="col-sm-7">
                        : <?=$data["nama_ibu"]?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nama Ayah</label>
                      <div class="col-sm-7">
                        : <?=$data["nama_ayah"]?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nama Balita</label>
                      <div class="col-sm-7">
                        : <?=$data["nama_balita"]?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Jenis Kelamin</label>
                      <div class="col-sm-7">
                        : <?php
                            $gender = "Perempuan";
                            if($data["jenis_kelamin"] == "L"){
                              $gender = "Laki-laki";
                            }
                            echo $gender;
                          ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Tanggal Lahir</label>
                      <div class="col-sm-7">
                        : <?=tgl_indo($data["tgl_lahir"])?>
                      </div>
                    </div>

                  </div> <!-- /.box-body -->

                  <div class="box-footer">
                  </div> <!-- /.box-footer -->
                </form>
              </div> <!-- /.box -->
            </div> <!--/.col-4 -->

            <div class="col-md-8">
              <!-- AREA CHART -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Pertumbuhan Berat Badan Balita</h3>

                  <div class="box-tools pull-right">
                    <a href="../balita/" class="btn btn-dark btn-sm hapus" title="Kembali Ke Halaman Semua Balita">
                      <i class="fa fa-arrow-left"></i> Kembali</a>
                  </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="areaChart" style="height:310px"></canvas>
                  </div>
                </div> <!-- /.box-body -->

                <?php
                $usia = json_encode($chartusia);
                $berat = json_encode($chartberat);

                     ?>

                    </div> <!-- /.box -->
            </div>
          </div> <!-- /.row -->

          <div class="row">
            <?php
            if($admin||$kader||$bidan){
              $id    = $_GET['id'];
              $conn  = koneksi();

              $hasils = mysqli_query($conn,"select MAX(usia) AS max_usia FROM detail_balita WHERE id_balita = '$id'");
              $datas = mysqli_fetch_array($hasils);

               ?>

            <div class="col-md-4 hapus">
              <!-- Horizontal Form# -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Pelayanan Balita</h3>
                </div> <!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" action="update.php" method="POST">
                  <input type="hidden" name="id_balita" value="<?=$_GET['id'];?>">
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Usia (Bulan)</label>
                      <div class="col-sm-7">
                        <input type="number" min="<?=$datas['max_usia']+1?>"id="usia-bayi" class="form-control" name="usia" placeholder="Usia Balita" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Berat (Kg)</label>
                      <div class="col-sm-7">
                        <input type="number" id="beratBadan"min="0" max="50" step="any" class="form-control" name="berat_badan" placeholder="Berat Balita" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Status Gizi</label>
                      <div class="col-sm-7">
                        <select  id="status" class="form-control" required disabled >
                          <option value="" readonly>Masukan Berat Balita</option>
                          <option value="1">Gizi Lebih</option>
                          <option value="2">Gizi Baik</option>
                          <option value="3">Gizi Kurang</option>
                          <option value="4">Gizi Buruk</option>
                        </select>
                      </div>
                    </div>
                    <input type="hidden" id="cekStatus" min="0" max="50" step="any" class="form-control" name="status_gizi" placeholder="cek status" required>
                    <?php
                    $disabled = "";
                    $text = "Hanya Bidan yang dapat mengakses Imunisasi, Vitamin, Obat";
                    if($kader){
                      $disabled = "disabled";
                      ?> <span class="small"><?=$text?></span><br><?php
                    }
                     ?>




                    <div class="form-group">
                      <label class="col-sm-4 control-label">Imunisasi</label>
                      <div class="col-sm-7">
                        <?php
                        $array_imunisasi = array(
                          'HB-0', 'BCG+Polio-1', 'DPT/HB1+Polio-2', 'DPT/HB2+Polio-3', 'DPT/HB3+Polio-4', 'Campak'
                        );
                        foreach($array_imunisasi as $value){
                          echo '<input type="checkbox" value='.$value.' name="imunisasi[]" '.$disabled.'/> '. $value.'<br>' ;
                        }
                         ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Vitamin A</label>
                      <div class="col-sm-7">
                      <input type="radio" name="vitamin" value="Kapsul Merah" <?=$disabled?>> Kapsul Merah<br>
                    <input type="radio"  name="vitamin" value="Kapsul Biru" <?=$disabled?>> Kapsul Biru
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Obat</label>
                      <div class="col-sm-7">
                        <div class="input-group">

                          <input type="text" class="form-control" name="obat" placeholder="obat" <?=$disabled?>>
                        </div>
                      </div>
                    </div>
                  </div> <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="reset" class="btn btn-default">Batal</button>
                    <input type="submit" name="simpan" class="btn btn-success pull-right"/>
                  </div> <!-- /.box-footer -->
                </form>
              </div> <!-- /.box -->
            </div> <!--/.col-4 -->
          <?php } ?>

            <div class="col-md-8" id="page-break">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title ">Tabel Pemeriksaan</h3>
                </div> <!-- /.box-header -->
          <div class="box-body">
              <div id="printableArea">
            <?php
              if($data["usia"] != "") {
            ?>
            <?php

            $hasil = mysqli_query($conn,"select * from detail_balita
                      inner join balita on detail_balita.id_balita = balita.id_balita
                      where balita.id_balita='$id'");
            $urut = mysqli_query($conn,"select max(nomor_urut) as max_urut from detail_balita
                                inner join balita on detail_balita.id_balita = balita.id_balita
                                where balita.id_balita='$id'");
            $max_urut = mysqli_fetch_array($urut);
            // echo $max_urut['max_urut'];
            $no_urut = 1;
                while ($data = mysqli_fetch_array($hasil)) {
                  $gizi = "Gizi Lebih";
                  $warna = "callout-info";
                  if($data["status_gizi"] == 2){
                    $gizi = "Gizi Baik";
                    $warna = "callout-success";
                  }elseif($data["status_gizi"] == 3){
                    $gizi = "Gizi Kurang";
                    $warna = "callout-warning";
                  }elseif($data["status_gizi"] == 4){
                    $gizi = "Gizi Buruk";
                    $warna = "callout-danger";
                  }
            ?>

              <div class="col-md-6" >
                <div class="text-left callout <?=$warna?> ">
                  <div class="hapus" style="float:right;">
                  <?php
                  if($no_urut == $max_urut['max_urut']){
                   ?>
                  <a href="hapus_detail.php?id=<?=$data['id_detail'];?>&balita=<?=$data['id_balita'];?>" style="color:grey;margin: 5px;" class="btn btn-default btn-sm pull-right" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  <?php
                }
                  if($bidan){

                   ?>
                  <a href="edit_pelayanan.php?id=<?=$data['id_detail'];?>&balita=<?=$data['id_balita'];?>" style="color:grey;margin: 5px;" class="btn btn-default btn-sm pull-right" title="Hapus Data"><i class="fa fa-edit"></i></a>
                <?php } ?>
                </div>
                  <h4 class="judul">Pemeriksaan Ke <?=$no_urut++;?></h4>

                  <?php
                  $imunisasi = $data['imunisasi'];


                   ?>
                  <p>
                    Usia : <?=$data["usia"]?> Bulan<br/>
                    Berat Badan : <?=$data["berat_badan"]?> Kg<br/>
                    Status Gizi : <?=$gizi?><br/>
                    Layanan Imunisasi :
                    <?=$imunisasi?><br/>

                    Vitamin : <?=$data["vitamin"]?><br/>
                    Obat : <?=$data["obat"]?><br/>
                    Tanggal : <?=tgl_indo($data["tgl_update"])?>
                  </p>
                </div> <!-- callout -->

              </div> <!--/.col-6 -->
              <?php
                  }
                  ?>
            </div>

                <button type="buttons" name="print" onclick="printDiv('printableArea')" class="btn btn-success btn-block pull-left hapus">Cetak Data</button>
              </div>

            </div>


            </div> <!--/.col-8 -->


          </div> <!-- row -->
          <?php
        } else{
        ?>
        <div class="row">
        <div class="text-center callout callout-danger">

          <h4>Belum ada data pemeriksaan</h4>

        </div>

      </div>
        <?php
        }
      ?>
        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

    <?php include_once('../footer.php'); ?>

  </div> <!-- ./wrapper -->

  <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../_assets/bower_components/chart.js/Chart.js"></script>
  <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../_assets/dist/js/adminlte.min.js"></script>
  <script src="../../_assets/bower_components/jspdf/jsPDF-1.3.2/dist/jspdf.min.js"></script>
  <script>
  $(document).ready(
    function(){
        //$('#hapus').removeClass('hide');

      $("#beratBadan").keyup(function(){
        // var beratBayi  = parseFloat($("#beratBadan").val());
        // //var diskon  = parseInt($("#diskon").val());
        var valueBerat = "";
        var usia = $("#usia-bayi").val();
        var beratBayi  = parseFloat($("#beratBadan").val());
        // console.log(usia);
        if(usia==0){
          if (beratBayi<=1.9){
            valueBerat = "4";
          }else if (beratBayi<=2.3 ){
            valueBerat = "3";
          }else if (beratBayi<=4.2){
            valueBerat = "2";
          }else if (beratBayi>4.2){
            valueBerat = "1";
          }
         $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
         $('#cekStatus').val(valueBerat);
         console.log(valueBerat);

       }
       else if(usia==1){
          if (beratBayi<=2.1){
            valueBerat = "4";
          }else if (beratBayi<=2.8 ){
            valueBerat = "3";
          }else if (beratBayi<=5.5){
            valueBerat = "2";
          }else if (beratBayi>5.5){
            valueBerat = "1";
          }
         $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
          $('#cekStatus').val(valueBerat);
         console.log(valueBerat);

        }
        else if(usia==2){
           if (beratBayi<=2.5){
             valueBerat = "4";
           }else if (beratBayi<=3.4 ){
             valueBerat = "3";
           }else if (beratBayi<=6.7){
             valueBerat = "2";
           }else if (beratBayi>6.7){
             valueBerat = "1";
           }
          $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
           $('#cekStatus').val(valueBerat);
          console.log(valueBerat);

         }
         else if(usia==3){
            if (beratBayi<=3.0){
              valueBerat = "4";
            }else if (beratBayi<=4.0 ){
              valueBerat = "3";
            }else if (beratBayi<=7.6){
              valueBerat = "2";
            }else if (beratBayi>7.6){
              valueBerat = "1";
            }
           $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
            $('#cekStatus').val(valueBerat);
           console.log(valueBerat);

          }
          else if(usia==4){
             if (beratBayi<=3.6){
               valueBerat = "4";
             }else if (beratBayi<=4.6 ){
               valueBerat = "3";
             }else if (beratBayi<=8.4){
               valueBerat = "2";
             }else if (beratBayi>8.4){
               valueBerat = "1";
             }
            $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
             $('#cekStatus').val(valueBerat);
            console.log(valueBerat);

           }
           else if(usia==5){
              if (beratBayi<=4.2){
                valueBerat = "4";
              }else if (beratBayi<=5.2 ){
                valueBerat = "3";
              }else if (beratBayi<=9.1){
                valueBerat = "2";
              }else if (beratBayi>9.1){
                valueBerat = "1";
              }
             $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
              $('#cekStatus').val(valueBerat);
             console.log(valueBerat);

            }
            else if(usia==6){
               if (beratBayi<=4.8){
                 valueBerat = "4";
               }else if (beratBayi<=5.8 ){
                 valueBerat = "3";
               }else if (beratBayi<=9.7){
                 valueBerat = "2";
               }else if (beratBayi>9.7){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==7){
               if (beratBayi<=5.3){
                 valueBerat = "4";
               }else if (beratBayi<=6.3 ){
                 valueBerat = "3";
               }else if (beratBayi<=10.2){
                 valueBerat = "2";
               }else if (beratBayi>10.2){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==8){
               if (beratBayi<=5.8){
                 valueBerat = "4";
               }else if (beratBayi<=6.8 ){
                 valueBerat = "3";
               }else if (beratBayi<=10.7){
                 valueBerat = "2";
               }else if (beratBayi>10.7){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==9){
               if (beratBayi<=6.2){
                 valueBerat = "4";
               }else if (beratBayi<=7.1 ){
                 valueBerat = "3";
               }else if (beratBayi<=11.2){
                 valueBerat = "2";
               }else if (beratBayi>11.2){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==10){
               if (beratBayi<=6.5){
                 valueBerat = "4";
               }else if (beratBayi<=7.5 ){
                 valueBerat = "3";
               }else if (beratBayi<=11.6){
                 valueBerat = "2";
               }else if (beratBayi>11.6){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==11){
               if (beratBayi<=6.8){
                 valueBerat = "4";
               }else if (beratBayi<=7.8 ){
                 valueBerat = "3";
               }else if (beratBayi<=11.9){
                 valueBerat = "2";
               }else if (beratBayi>11.9){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==12){
               if (beratBayi<=7.0){
                 valueBerat = "4";
               }else if (beratBayi<=8.0 ){
                 valueBerat = "3";
               }else if (beratBayi<=12.3){
                 valueBerat = "2";
               }else if (beratBayi>12.3){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==13){
               if (beratBayi<=7.2){
                 valueBerat = "4";
               }else if (beratBayi<=8.2 ){
                 valueBerat = "3";
               }else if (beratBayi<=12.6){
                 valueBerat = "2";
               }else if (beratBayi>12.6){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==14){
               if (beratBayi<=7.4){
                 valueBerat = "4";
               }else if (beratBayi<=8.4 ){
                 valueBerat = "3";
               }else if (beratBayi<=12.9){
                 valueBerat = "2";
               }else if (beratBayi>12.9){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==15){
               if (beratBayi<=7.5){
                 valueBerat = "4";
               }else if (beratBayi<=8.6 ){
                 valueBerat = "3";
               }else if (beratBayi<=13.1){
                 valueBerat = "2";
               }else if (beratBayi>13.1){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==16){
               if (beratBayi<=7.6){
                 valueBerat = "4";
               }else if (beratBayi<=8.7 ){
                 valueBerat = "3";
               }else if (beratBayi<=13.4){
                 valueBerat = "2";
               }else if (beratBayi>13.4){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==17){
               if (beratBayi<=7.7){
                 valueBerat = "4";
               }else if (beratBayi<=8.9 ){
                 valueBerat = "3";
               }else if (beratBayi<=13.6){
                 valueBerat = "2";
               }else if (beratBayi>13.6){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==18){
               if (beratBayi<=7.8){
                 valueBerat = "4";
               }else if (beratBayi<=9.0 ){
                 valueBerat = "3";
               }else if (beratBayi<=13.8){
                 valueBerat = "2";
               }else if (beratBayi>13.8){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==19){
               if (beratBayi<=7.9){
                 valueBerat = "4";
               }else if (beratBayi<=9.1 ){
                 valueBerat = "3";
               }else if (beratBayi<=14.0){
                 valueBerat = "2";
               }else if (beratBayi>14.0){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==20){
               if (beratBayi<=8.0){
                 valueBerat = "4";
               }else if (beratBayi<=9.3 ){
                 valueBerat = "3";
               }else if (beratBayi<=14.3){
                 valueBerat = "2";
               }else if (beratBayi>14.3){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==21){
               if (beratBayi<=8.2){
                 valueBerat = "4";
               }else if (beratBayi<=9.4 ){
                 valueBerat = "3";
               }else if (beratBayi<=14.5){
                 valueBerat = "2";
               }else if (beratBayi>14.5){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==22){
               if (beratBayi<=8.3){
                 valueBerat = "4";
               }else if (beratBayi<=9.6 ){
                 valueBerat = "3";
               }else if (beratBayi<=14.7){
                 valueBerat = "2";
               }else if (beratBayi>14.7){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==23){
               if (beratBayi<=8.4){
                 valueBerat = "4";
               }else if (beratBayi<=9.7 ){
                 valueBerat = "3";
               }else if (beratBayi<=14.9){
                 valueBerat = "2";
               }else if (beratBayi>14.9){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==24){
               if (beratBayi<=8.9){
                 valueBerat = "4";
               }else if (beratBayi<=10.0 ){
                 valueBerat = "3";
               }else if (beratBayi<=15.6){
                 valueBerat = "2";
               }else if (beratBayi>15.6){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==25){
               if (beratBayi<=8.9){
                 valueBerat = "4";
               }else if (beratBayi<=10.1 ){
                 valueBerat = "3";
               }else if (beratBayi<=15.8){
                 valueBerat = "2";
               }else if (beratBayi>15.8){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==26){
               if (beratBayi<=9.0){
                 valueBerat = "4";
               }else if (beratBayi<=10.2 ){
                 valueBerat = "3";
               }else if (beratBayi<=16.0){
                 valueBerat = "2";
               }else if (beratBayi>16.0){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==27){
               if (beratBayi<=9.0){
                 valueBerat = "4";
               }else if (beratBayi<=10.3 ){
                 valueBerat = "3";
               }else if (beratBayi<=16.2){
                 valueBerat = "2";
               }else if (beratBayi>16.2){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==28){
               if (beratBayi<=9.1){
                 valueBerat = "4";
               }else if (beratBayi<=10.4 ){
                 valueBerat = "3";
               }else if (beratBayi<=16.5){
                 valueBerat = "2";
               }else if (beratBayi>16.5){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==29){
               if (beratBayi<=9.2){
                 valueBerat = "4";
               }else if (beratBayi<=10.5 ){
                 valueBerat = "3";
               }else if (beratBayi<=16.7){
                 valueBerat = "2";
               }else if (beratBayi>16.7){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==30){
               if (beratBayi<=9.3){
                 valueBerat = "4";
               }else if (beratBayi<=10.6 ){
                 valueBerat = "3";
               }else if (beratBayi<=16.9){
                 valueBerat = "2";
               }else if (beratBayi>16.9){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==31){
               if (beratBayi<=9.3){
                 valueBerat = "4";
               }else if (beratBayi<=10.8 ){
                 valueBerat = "3";
               }else if (beratBayi<=17.1){
                 valueBerat = "2";
               }else if (beratBayi>17.1){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==32){
               if (beratBayi<=9.4){
                 valueBerat = "4";
               }else if (beratBayi<=10.9 ){
                 valueBerat = "3";
               }else if (beratBayi<=17.3){
                 valueBerat = "2";
               }else if (beratBayi>17.3){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==33){
               if (beratBayi<=9.5){
                 valueBerat = "4";
               }else if (beratBayi<=11.0 ){
                 valueBerat = "3";
               }else if (beratBayi<=17.5){
                 valueBerat = "2";
               }else if (beratBayi>17.5){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==34){
               if (beratBayi<=9.6){
                 valueBerat = "4";
               }else if (beratBayi<=11.1 ){
                 valueBerat = "3";
               }else if (beratBayi<=17.7){
                 valueBerat = "2";
               }else if (beratBayi>17.7){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==35){
               if (beratBayi<=9.6){
                 valueBerat = "4";
               }else if (beratBayi<=11.2 ){
                 valueBerat = "3";
               }else if (beratBayi<=17.9){
                 valueBerat = "2";
               }else if (beratBayi>17.9){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==36){
               if (beratBayi<=9.7){
                 valueBerat = "4";
               }else if (beratBayi<=11.3 ){
                 valueBerat = "3";
               }else if (beratBayi<=18.2){
                 valueBerat = "2";
               }else if (beratBayi>18.2){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==37){
               if (beratBayi<=9.8){
                 valueBerat = "4";
               }else if (beratBayi<=11.4 ){
                 valueBerat = "3";
               }else if (beratBayi<=18.4){
                 valueBerat = "2";
               }else if (beratBayi>18.4){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==38){
               if (beratBayi<=9.9){
                 valueBerat = "4";
               }else if (beratBayi<=11.6 ){
                 valueBerat = "3";
               }else if (beratBayi<=18.6){
                 valueBerat = "2";
               }else if (beratBayi>18.6){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==39){
               if (beratBayi<=10.0){
                 valueBerat = "4";
               }else if (beratBayi<=11.7 ){
                 valueBerat = "3";
               }else if (beratBayi<=18.8){
                 valueBerat = "2";
               }else if (beratBayi>18.8){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==40){
               if (beratBayi<=10.1){
                 valueBerat = "4";
               }else if (beratBayi<=11.8 ){
                 valueBerat = "3";
               }else if (beratBayi<=19.0){
                 valueBerat = "2";
               }else if (beratBayi>19.0){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==41){
               if (beratBayi<=10.2){
                 valueBerat = "4";
               }else if (beratBayi<=11.9 ){
                 valueBerat = "3";
               }else if (beratBayi<=19.2){
                 valueBerat = "2";
               }else if (beratBayi>19.2){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==42){
               if (beratBayi<=10.3){
                 valueBerat = "4";
               }else if (beratBayi<=12.0 ){
                 valueBerat = "3";
               }else if (beratBayi<=19.4){
                 valueBerat = "2";
               }else if (beratBayi>19.4){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==43){
               if (beratBayi<=10.4){
                 valueBerat = "4";
               }else if (beratBayi<=12.2 ){
                 valueBerat = "3";
               }else if (beratBayi<=19.6){
                 valueBerat = "2";
               }else if (beratBayi>19.6){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==44){
               if (beratBayi<=10.5){
                 valueBerat = "4";
               }else if (beratBayi<=12.3 ){
                 valueBerat = "3";
               }else if (beratBayi<=19.8){
                 valueBerat = "2";
               }else if (beratBayi>19.8){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==45){
               if (beratBayi<=10.6){
                 valueBerat = "4";
               }else if (beratBayi<=12.4 ){
                 valueBerat = "3";
               }else if (beratBayi<=20.0){
                 valueBerat = "2";
               }else if (beratBayi>20.0){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==46){
               if (beratBayi<=10.7){
                 valueBerat = "4";
               }else if (beratBayi<=12.5 ){
                 valueBerat = "3";
               }else if (beratBayi<=20.3){
                 valueBerat = "2";
               }else if (beratBayi>20.3){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==47){
               if (beratBayi<=10.8){
                 valueBerat = "4";
               }else if (beratBayi<=12.7 ){
                 valueBerat = "3";
               }else if (beratBayi<=20.5){
                 valueBerat = "2";
               }else if (beratBayi>20.5){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==48){
               if (beratBayi<=10.9){
                 valueBerat = "4";
               }else if (beratBayi<=12.8 ){
                 valueBerat = "3";
               }else if (beratBayi<=20.7){
                 valueBerat = "2";
               }else if (beratBayi>20.7){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==49){
               if (beratBayi<=11.0){
                 valueBerat = "4";
               }else if (beratBayi<=12.9 ){
                 valueBerat = "3";
               }else if (beratBayi<=20.9){
                 valueBerat = "2";
               }else if (beratBayi>20.9){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==50){
               if (beratBayi<=11.1){
                 valueBerat = "4";
               }else if (beratBayi<=13.0 ){
                 valueBerat = "3";
               }else if (beratBayi<=21.1){
                 valueBerat = "2";
               }else if (beratBayi>21.1){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==51){
               if (beratBayi<=11.2){
                 valueBerat = "4";
               }else if (beratBayi<=13.2 ){
                 valueBerat = "3";
               }else if (beratBayi<=21.3){
                 valueBerat = "2";
               }else if (beratBayi>21.3){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==52){
               if (beratBayi<=11.3){
                 valueBerat = "4";
               }else if (beratBayi<=13.3 ){
                 valueBerat = "3";
               }else if (beratBayi<=21.6){
                 valueBerat = "2";
               }else if (beratBayi>21.6){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==53){
               if (beratBayi<=11.4){
                 valueBerat = "4";
               }else if (beratBayi<=13.4 ){
                 valueBerat = "3";
               }else if (beratBayi<=21.8){
                 valueBerat = "2";
               }else if (beratBayi>21.8){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==54){
               if (beratBayi<=11.5){
                 valueBerat = "4";
               }else if (beratBayi<=13.6 ){
                 valueBerat = "3";
               }else if (beratBayi<=22.0){
                 valueBerat = "2";
               }else if (beratBayi>22.0){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==55){
               if (beratBayi<=11.7){
                 valueBerat = "4";
               }else if (beratBayi<=13.7 ){
                 valueBerat = "3";
               }else if (beratBayi<=22.2){
                 valueBerat = "2";
               }else if (beratBayi>22.2){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==56){
               if (beratBayi<=11.8){
                 valueBerat = "4";
               }else if (beratBayi<=13.8 ){
                 valueBerat = "3";
               }else if (beratBayi<=22.5){
                 valueBerat = "2";
               }else if (beratBayi>22.5){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==57){
               if (beratBayi<=11.9){
                 valueBerat = "4";
               }else if (beratBayi<=14.0 ){
                 valueBerat = "3";
               }else if (beratBayi<=22.7){
                 valueBerat = "2";
               }else if (beratBayi>22.7){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==58){
               if (beratBayi<=12.0){
                 valueBerat = "4";
               }else if (beratBayi<=14.1 ){
                 valueBerat = "3";
               }else if (beratBayi<=22.9){
                 valueBerat = "2";
               }else if (beratBayi>22.9){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);

            }
            else if(usia==59){
               if (beratBayi<=12.1){
                 valueBerat = "4";
               }else if (beratBayi<=14.2 ){
                 valueBerat = "3";
               }else if (beratBayi<=23.2){
                 valueBerat = "2";
               }else if (beratBayi>23.2){
                 valueBerat = "1";
               }
              $('#status option').removeAttr('selected').filter('[value=' + valueBerat + ']').attr('selected', true);
               $('#cekStatus').val(valueBerat);
              console.log(valueBerat);
            }
          });
    });
  $('#downloadPdf').click(function(event) {
  // get size of report page
  var reportPageHeight = $('#printableArea').innerHeight();
  var reportPageWidth = $('#printableArea').innerWidth();

  // create a new canvas object that we will populate with all other canvas objects
  var pdfCanvas = $('<canvas />').attr({
    id: "canvaspdf",
    width: reportPageWidth,
    height: reportPageHeight
  });

  // keep track canvas position
  var pdfctx = $(pdfCanvas)[0].getContext('2d');
  var pdfctxX = 10;
  var pdfctxY = 10;
  var buffer = 100;

  // for each chart.js chart
  $("canvas").each(function(index) {
    // get the chart height/width
    var canvasHeight = $(this).innerHeight();
    var canvasWidth = $(this).innerWidth();

    // draw the chart into the new canvas
    pdfctx.drawImage($(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
    pdfctxX += canvasWidth + buffer;

    // our report page is in a grid pattern so replicate that in the new canvas
    if (index % 2 === 1) {
      pdfctxX = 0;
      pdfctxY += canvasHeight + buffer;
    }
  });

  // create new pdf and add our new canvas as an image
  var pdf = new jsPDF('l', 'pt', [reportPageWidth, reportPageHeight]);
  pdf.addImage($(pdfCanvas)[0], 'PNG', 0, 0);

  // download the pdf
  pdf.save('grafik.pdf');
});
  function printDiv(divName) {

    $('.hapus').addClass('hide');

     window.print();

     location.reload();
     return false;
	}
/*
  //Download Chart Image
document.getElementById("unduh").addEventListener('click', function(){
  /*Get image of canvas element
  var url_base64jp = document.getElementById("areaChart").toDataURL();
  /*get download button (tag: <a></a>)
  var a =  document.getElementById("unduh");
  /*insert chart image url to download button (tag: <a></a>)
  a.href = url_base64jp;
});
  //Download Chart Image
document.getElementById("download").addEventListener('click', function(){
  /*Get image of canvas element
  var canvas = document.querySelector('#areaChart');
 //creates image
 var canvasImg = canvas.toDataURL("image/jpeg", 1.0);

 //creates PDF from img
 var doc = new jsPDF('landscape');
 doc.setFontSize(20);
 doc.text(15, 15, "Cool Chart");
 doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150 );
 doc.save('canvas.pdf');
});
*/
    $(function () {
      /* ChartJS
       * -------
       * Here we will create a few charts using ChartJS
       */

      //--------------
      //- AREA CHART -
      //--------------

      // // Get context with jQuery - using jQuery's .get() method.
      // var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
      // // This will get the first returned node in the jQuery collection.
      // var areaChart       = new Chart(areaChartCanvas)

      var areaChartData = {
        labels  : <?=json_encode($chartusia)?>,
        datasets: [
          {
            label               : 'Digital Goods',
            fillColor           : 'rgba(60,141,188,0.9)',
            strokeColor         : 'rgba(60,141,188,0.8)',
            pointColor          : '#3b8bba',
            pointStrokeColor    : 'rgba(60,141,188,1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data                : <?=json_encode($chartberat)?>
          }
        ]
      }

      var areaChartOptions = {

        backgroundColor:'rgb(10,10,10)',
        showTooltips: false,
        onAnimationProgress: function() { drawDatasetPointsLabels() },
        onAnimationComplete: function() { drawDatasetPointsLabels() },
        // Boolean - If we should show the scale at all
        showScale               : true,
        scaleOverride:true,
  scaleSteps:6,
  scaleStartValue:0,
  scaleStepWidth:4,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : true,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,

        //Boolean - Whether the line is curved between points
        bezierCurve             : true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension      : 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot                : true,
        //Number - Radius of each point dot in pixels
        pointDotRadius          : 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth     : 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke           : true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth      : 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill             : false,
        //String - A legend template
        maintainAspectRatio     : true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive              : true
      }

      //areaChart.Line(areaChartData, areaChartOptions)

      var ctx = $("#areaChart").get(0).getContext("2d");
            // var Trends = new Chart(ctx).Line(areaChartData, areaChartOptions);
            var Trends = new Chart(ctx).Line(areaChartData, areaChartOptions);


        function drawDatasetPointsLabels() {
            ctx.font = '12px "Gotham Book",sans-serif';
            ctx.fillStyle = '#AAA';
            ctx.textAlign="center";
            $(Trends.datasets).each(function(idx,dataset){
                // First dataset is shifted off the scale line.
                // Don't write to the canvas for the null placeholder.
                $(dataset.points).each(function(pdx,pointinfo){
                    if ( pointinfo.value !== null ) {
                        ctx.fillText(pointinfo.value,pointinfo.x,pointinfo.y - 15);
                    }
                });
            });
        }
    });
  </script>
</body>
</html>
