<?php
  require_once '../../koneksi.php';
require_once '../../_assets/_fungsiTanggal.php';


  if(isset($_POST["simpan"])){
    $nokk   = $_POST['nomor_kk'];
    $nama   = $_POST['nama_balita'];
    $gender = $_POST['jenis_kelamin'];
    $tgl    = $_POST['tgl_lahir'];
    $berat_lahir   = $_POST['berat'];
    $panjang_lahir   = $_POST['panjang'];

    $conn  = koneksi();
    $sql   = "insert into balita (nomor_kk,nama_balita,jenis_kelamin,tgl_lahir,berat_lahir,panjang_lahir)
              values('$nokk','$nama','$gender','$tgl','$berat_lahir','$panjang_lahir')";
    $hasil = mysqli_query($conn, $sql);

    $select = mysqli_query($conn,"select max(id_balita) as maksimum from balita");
    $data   = mysqli_fetch_array($select);

    $idd    = $data['maksimum'];
    $update = date('Y-m-d');
    $sql2   = "insert into detail_balita (id_balita,tgl_update) values('$idd','$update')";
    $hasil2 = mysqli_query($conn, $sql2);

    if(!$hasil) {
      echo "<script>alert('Gagal Simpan')</script>";
      //echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
    else {
      echo "<script>alert('Data Berhasil Disimpan')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
  }
 include_once('../../header.php');

 $cek =  $_SESSION['level'];
 $id_user = $_SESSION['id'];
 $admin = false;
 $kader = false;
 $bidan = false;
 $ortu = false;
 if ($cek == 0){
     $admin = true;
 }elseif ($cek == 1){

   $kader = true;
 }elseif ($cek == 2) {
   // code...

   $bidan = true;

}elseif ($cek == 3) {
 // code...

 $ortu = true;
}

//  var_dump($cek);
?>

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Semua Balita
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> <?php echo $_SESSION['username'] ?></a></li>
            <li class="active">Semua Balita</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-4">
            <!-- Horizontal Form -->
            <?php if ($admin || $kader || $bidan){ ?>
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambah Balita</h3>
                </div> <!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST">
                  <div class="box-body">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nomor KK</label>
                      <div class="col-sm-7">
                        <select class="form-control select2" name="nomor_kk" required>
                          <option value="" readonly >Pilih Kartu Keluarga</option>
                          <?php
                            $conn = koneksi();
                            $sql  ="select * from orang_tua";
                            $hasil = mysqli_query($conn, $sql);
                            while ($r = mysqli_fetch_array($hasil)) {
                              $valueKk=$r['nomor_kk'];
                              $nama=$r['nama_ibu'];
                          ?>
                          <option value="<?=$r['nomor_kk']?>"><?php echo "No KK : ".$valueKk . ", Nama Ibu: " . $nama?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nama Balita</label>
                      <div class="col-sm-7">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-child"></i>
                          </div>
                          <input type="text" class="form-control" name="nama_balita" placeholder="Nama Balita" required>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Jenis Kelamin</label>
                      <div class="col-sm-7">
                        <select class="form-control select2" name="jenis_kelamin" required>
                          <option value="" readonly >Pilih Jenis Kelamin</option>
                          <option value="L">Laki-laki</option>
                          <option value="P">Perempuan</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Tanggal Lahir</label>
                      <div class="col-sm-7">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal Lahir" required>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Berat Lahir</label>
                      <div class="col-sm-7">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-child"></i>
                          </div>
                          <input type="number" min="0" max="50" step="any" class="form-control" name="berat" placeholder="Berat Lahir" required>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Panjang Lahir</label>
                      <div class="col-sm-7">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-child"></i>
                          </div>
                          <input type="number" min="0" max="70" step="any" class="form-control" name="panjang" placeholder="Nama Balita" required>
                        </div>
                      </div>
                    </div>
                  </div> <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="reset" class="btn btn-default">Batal</button>
                    <button type="submit" name="simpan" class="btn btn-success pull-right">Simpan</button>
                  </div> <!-- /.box-footer -->
                </form>
              </div> <!-- /.box -->
              <?php } ?>
            </div> <!--/.col-4 -->
            <?php if ($ortu){ ?>
              <div class="row">
                <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Data Balita Saya</h3>

                  </div> <!-- /.box-header -->

                  <div class="box-body">

                    <div id="printableArea">

                    <table id="posyandu" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Balita</th>
                          <th>Nama Ibu</th>
                          <th>Jenis Kelamin</th>
                          <th>Tanggal Lahir</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $conn = koneksi();
                          $sql =  "select * from user inner join orang_tua as user_ortu on user.id_user = user_ortu.id_user AND user.id_user = '$id_user' inner join balita on balita.nomor_kk=user_ortu.nomor_kk";

                          $hasil = mysqli_query($conn, $sql);
                          $no    = 1;
                          while ($data = mysqli_fetch_array($hasil)) {
                            $gender="";
                            if($data["jenis_kelamin"]=="L"){
                                $gender="Laki-laki";
                            }
                            if($data["jenis_kelamin"]=="P"){
                                $gender="Perempuan";
                            }


                        ?>
                        <tr>
                          <td><?=$no++?></td>
                          <td><?=$data["nama_balita"]?></td>
                          <td><?=$data["nama_ibu"]?></td>
                          <td><?=$gender?></td>
                          <td><?=tgl_indo($data["tgl_lahir"])?></td>
                          <td>

                          <a href="detail.php?id=<?=$data['id_balita'];?>" class="btn btn-sm btn-primary" title="Detail Data"><i class="fa fa-search"></i></a>

                          </td>
                        </tr>
                        <?php } ?>

                      </tbody>
                    </table>
                  </div>

                    </div> <!-- /.box-body -->

                </div> <!-- /.box -->
              </div><?php }else{ ?>
            <div class="col-md-8">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Semua Balita</h3>

                </div> <!-- /.box-header -->

                <div class="box-body">

                  <div id="printableArea">
                  <div class="hide text-center" id="table-header">
                    <p><strong>Laporan Data Balita</strong><br>
                    Bulan - Tahun <br><span id="value-header"></span></p>
                  </div>
                  <table id="posyandu" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Balita</th>
                        <th>Nama Ibu</th>
                        <th>Jenis Kelamin</th>
                        <th>Tanggal Lahir</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $conn = koneksi();
                        $sql =  "select * from balita
                                  inner join orang_tua on balita.nomor_kk=orang_tua.nomor_kk";

                        $hasil = mysqli_query($conn, $sql);
                        $no    = 1;
                        while ($data = mysqli_fetch_array($hasil)) {
                          $gender="";
                          if($data["jenis_kelamin"]=="L"){
                              $gender="Laki-laki";
                          }
                          if($data["jenis_kelamin"]=="P"){
                              $gender="Perempuan";
                          }


                      ?>
                      <tr>
                        <td><?=$no++?></td>
                        <td><?=$data["nama_balita"]?></td>
                        <td><?=$data["nama_ibu"]?></td>
                        <td><?=$gender?></td>
                        <td><?=tgl_indo($data["tgl_lahir"])?></td>
                        <td>
                          <?php if ($admin){ ?>
                          <a href="detail.php?id=<?=$data['id_balita'];?>" class="btn btn-sm btn-primary" title="Detail Data"><i class="fa fa-search"></i></a>
                          <a href="edit.php?id=<?=$data['id_balita'];?>" class="btn btn-sm btn-warning" title="Edit Data"><i class="fa fa-pencil"></i></a>
                          <a href="hapus.php?id=<?=$data['id_balita'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')" title="Hapus Data"><i class="fa fa-trash"></i></a>
                        <?php } if ($bidan||$kader){ ?>
                        <a href="detail.php?id=<?=$data['id_balita'];?>" class="btn btn-sm btn-primary" title="Detail Data"><i class="fa fa-search"></i></a>
                        <a href="edit.php?id=<?=$data['id_balita'];?>" class="btn btn-sm btn-warning" title="Edit Data"><i class="fa fa-pencil"></i></a>
                      <?php }  ?>
                        </td>
                      </tr>
                      <?php } ?>

                    </tbody>
                  </table>
                </div>
                <!-- <button type="submit" name="print" onclick="printDiv('printableArea')" class="btn btn-success pull-right">Cetak Laporan Data Balita</button><br><br>
                <p class="small text-right">
                  *Masukan nama bulan dan tahun kedalam kolom search<br>jika ingin mencetak laporan berdasarkan Bulan<br>
                  (contoh : Januari 2020)
                </p> -->
                  </div> <!-- /.box-body -->

              </div> <!-- /.box -->
            <?php } ?>
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->

        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->


  </div> <!-- ./wrapper -->
<?php include_once('../footer.php'); ?>
  <script>

    $(function () {

      $('#datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight:true
      })
      $('.select2').select2()
      $('#posyandu').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : false,
        'info'        : true,
        'autoWidth'   : true,
        'columnDefs': [

          {
            "targets": [ 5 ],
            "searchable": false,
            "orderable": false
          },
          {
            "targets": [ 0 ],
            "searchable": false
          },
        ]
      })
    })
    function printDiv(divName) {
      var table = $('#posyandu').DataTable();

      table.column( 5 ).visible( false );

      var filter = $('input[type=search]').val();
      // console.log (filter);

      document.getElementById("value-header").innerHTML=filter;

      $('#posyandu_paginate').addClass('hide');
      $('#posyandu_filter').addClass('hide');
      $('#table-header').removeClass('hide');
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
       location.reload();
       return false;
    }
  </script>
</body>
</html>
