<?php
  include_once('../koneksi.php');

  session_start();
  if(!isset($_SESSION['username'])){
      header('Location:../login.php');
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Posyandu</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../_assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../_assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- <link rel="stylesheet" href="../_assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"> -->
  <link rel="stylesheet" href="../_assets/bower_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="../_assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../_assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <!--DataTables -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" />

  <!-- Pace Loader -->
  <link rel="stylesheet" href="../_assets/pace/pace-white-minimal.css" />
  <script src="../_assets/pace/pace.min.js"></script>

</head>

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">
    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <a href="./" class="navbar-brand"><b></b> KMS</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="./">Beranda</a></li>
              <?php
            $cek =  $_SESSION['level'];
            $admin = false;
            $kader = false;
            $bidan = false;
            $ortu = false;
            if ($cek == 0){
                $admin = true;
            }elseif ($cek == 1){

              $kader = true;
            }elseif ($cek == 2) {
              // code...

              $bidan = true;

          }elseif ($cek == 3) {
            // code...

            $ortu = true;
          }


            if ($kader){


                 ?>

                <li><a href="orang-tua/">Orang Tua</a></li>
                <li >

                    <li><a href="balita/">Semua Balita</a></li>


                </li>
                <li >

                    <li><a href="laporan/laporan_balita.php">Laporan</a></li>


                </li>
                <li><a href="user/">User</a></li>
            <?php  }
            if ($bidan){


                 ?>

                <li><a href="orang-tua/">Orang Tua</a></li>


                    <li><a href="balita/">Semua Balita</a></li>


              
                <li >

                    <li><a href="laporan/laporan_balita.php">Laporan</a></li>


                </li>

            <?php  }
            if ($ortu){


                 ?>


                <li >

                    <li><a href="balita/">Semua Balita</a></li>


                </li>

            <?php  }?>



              <li><a href="../logout.php" title="Keluar">Keluar <i class="fa fa-sign-out"></i></a></li>

            </ul>
          </div> <!-- /.navbar-collapse -->
        </div> <!-- /.container-fluid -->
      </nav>
    </header>
