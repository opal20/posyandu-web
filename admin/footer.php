<footer class="main-footer">
  <div class="container">
    <div class="pull-right hidden-xs">
      version 1.0.0 | <a href="" title="Developer">Kartu Menuju Sehat</a>
    </div>
    <strong>Copyright &copy; 2020 <a href="" target="_blank">Kartu Menuju Sehat</a>.</strong>
  </div> <!-- /.container -->
</footer>
  </div> <!-- ./wrapper -->

    <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../_assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../_assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../_assets/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="../../_assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <!--DataTables -->
   <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <script src="../../_assets/dist/js/adminlte.min.js"></script>

</body>
</html>
