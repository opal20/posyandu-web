<?php
  require_once '../../koneksi.php';



  if(isset($_POST["simpan"])){
    $nama   = $_POST['nama_posyandu'];
    $alamat = $_POST['alamat'];

    $conn  = koneksi();
    $sql   = "insert into posyandu (nama_posyandu,alamat) values('$nama','$alamat')";
    $hasil = mysqli_query($conn, $sql);
    if(!$hasil) {
      echo "<script>alert('Gagal Simpan')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
    else {
      echo "<script>alert('Data Berhasil Disimpan')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
  }
?>

<!DOCTYPE html>
<html>
<?php include_once('../../header.php'); ?>
    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Posyandu
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Posyandu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-4">
            <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambah Posyandu</h3>
                </div> <!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nama</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="nama_posyandu" placeholder="Nama Posyandu" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Alamat</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="alamat" placeholder="Alamat" required>
                      </div>
                    </div>
                  </div> <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="reset" class="btn btn-default">Batal</button>
                    <button type="submit" name="simpan" class="btn btn-success pull-right">Simpan</button>
                  </div> <!-- /.box-footer -->
                </form>
              </div> <!-- /.box -->
            </div> <!--/.col-4 -->

            <div class="col-md-8">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Posyandu</h3>
                </div> <!-- /.box-header -->

                <div class="box-body">
                  <table id="posyandu" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Posyandu</th>
                        <th>Alamat</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $conn = koneksi();
                        $sql =  "select * from posyandu";

                        $hasil = mysqli_query($conn, $sql);
                        $no    = 1;
                        while ($data = mysqli_fetch_array($hasil)) {
                      ?>
                      <tr>
                        <td><?=$no++?></td>
                        <td><?=$data["nama_posyandu"]?></td>
                        <td><?=$data["alamat"]?></td>
                        <td>
                          <a href="edit.php?id_posyandu=<?=$data['id_posyandu'];?>" class="btn btn-sm btn-warning" title="Edit Data"><i class="fa fa-pencil"></i></a>
                          <a href="hapus.php?id_posyandu=<?=$data['id_posyandu'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')" title="Hapus Data"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                      <?php } ?>

                    </tbody>
                  </table>
                </div> <!-- /.box-body -->

              </div> <!-- /.box -->
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->

        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="container">
        <div class="pull-right hidden-xs">
          version 1.0.0 | Developed by <a href="" title="Developer">Abdul Hafizh</a>
        </div>
        <strong>Copyright &copy; 2018 <a href="https://adminlte.io" target="_blank">Almsaeed Studio</a>.</strong>
      </div> <!-- /.container -->
    </footer>
  </div> <!-- ./wrapper -->

  <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../_assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../../_assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="../../_assets/dist/js/adminlte.min.js"></script>
  <script>
    $(function () {
      $('#posyandu').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'info'        : true,
        'autoWidth'   : true,
        'columnDefs': [
          {
            "targets": [ 2,3 ],
            "orderable": false,
            "searchable": false
          },
          {
            "targets": [ 0 ],
            "searchable": false
          }
        ]
      })
    })
  </script>
</body>
</html>
