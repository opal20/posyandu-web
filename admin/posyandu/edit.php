<?php 
  require_once '../../koneksi.php';

  session_start();
  if(!isset($_SESSION['username'])){    
      header('Location:../../login.php');
  }  

  if(isset($_POST['update'])){
    $id     = $_POST["id_posyandu"];
    $nama   = $_POST["nama_posyandu"];
    $alamat = $_POST["alamat"];
    $conn   = koneksi();    

    $hasil  = mysqli_query($conn, "update posyandu set nama_posyandu='$nama', alamat='$alamat' where id_posyandu='$id'");
    if(!$hasil) {
      echo "<script>alert('Gagal Ubah Data')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>"; 
    }
    else {
      echo "<script>alert('Data Berhasil Diubah')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>"; 
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Posyandu</title>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../_assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../_assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../_assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="../../_assets/bower_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="../../_assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../_assets/dist/css/skins/_all-skins.min.css">

  <!-- Pace Loader -->
  <link rel="stylesheet" href="../../_assets/pace/pace-white-minimal.css" />
  <script src="../../_assets/pace/pace.min.js"></script>
  
</head>

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">
    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <a href="../" class="navbar-brand"><b>Admin</b> Posyandu</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">              
              <li><a href="../">Beranda</a></li>
              <li><a href="./">Posyandu</a></li>
              <li><a href="../orang-tua/">Orang Tua</a></li>
              <li class="dropdwon">
                <a class="dropdown-toggle" data-toggle="dropdown">Balita <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="../balita/">Semua Balita</a></li>
                  <li><a href="../balita/data-latih.php">Data Latih (Training)</a></li>
                  <li><a href="../balita/data-uji.php">Data Uji (Testing)</a></li>
                  <li><a href="../balita/akurasi.php">Akurasi</a></li>
                </ul>
              </li>              
              <li><a href="../user/">User</a></li>              
              <li><a href="../../logout.php" title="Keluar">Keluar <i class="fa fa-sign-out"></i></a></li>
              
            </ul>
          </div> <!-- /.navbar-collapse -->
        </div> <!-- /.container-fluid -->
      </nav>
    </header>

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Posyandu
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Posyandu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-8">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Data Posyandu</h3>
                </div> <!-- /.box-header -->

                <div class="box-body">
                  <!-- form start -->
                  <?php
                    $id    = $_GET['id_posyandu'];
                    $conn  = koneksi();
                    $hasil = mysqli_query($conn,"select * from posyandu where id_posyandu='$id'");
                    $data  = mysqli_fetch_array($hasil);
                  ?>
                  <form class="form-horizontal" method="POST">
                    <input type="hidden" name="id_posyandu" value="<?=$data['id_posyandu']?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Nama Posyandu</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="nama_posyandu" placeholder="Nama Posyandu" value="<?=$data['nama_posyandu']?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Alamat Posyandu</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="alamat" placeholder="Alamat" value="<?=$data['alamat']?>" required>
                        </div>
                      </div>
                    </div> <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" name="update" class="btn btn-warning pull-right"><i class="fa fa-save"></i> Simpan</button>
                    </div> <!-- /.box-footer -->
                  </form>
                </div> <!-- /.box-body -->               
              </div> <!-- /.box -->          
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->
         
        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="container">
        <div class="pull-right hidden-xs">
          version 1.0.0 | Developed by <a href="" title="Developer">Abdul Hafizh</a>
        </div>
        <strong>Copyright &copy; 2018 <a href="https://adminlte.io" target="_blank">Almsaeed Studio</a>.</strong>
      </div> <!-- /.container -->
    </footer>
  </div> <!-- ./wrapper -->

  <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../_assets/dist/js/adminlte.min.js"></script>
</body>
</html>