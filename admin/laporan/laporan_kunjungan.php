
 <?php include_once('../../header.php'); ?>
<div class="content-wrapper">
  <div class="container">
    <section class="content-header">
      <h1>
        Semua Balita
        <small>Kelola Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="../"><i class="fa fa-dashboard"></i> <?php echo $_SESSION['username'] ?></a></li>
        <li class="active">Semua Balita</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div id="printableArea">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Laporan Balita</h3>

            </div> <!-- /.box-header -->

            <div class="box-body">

        <div class="row">
         <div class="input-daterange hapus">
          <div class="col-md-4">
           <input type="text" name="start_date" id="start_date" class="form-control" placeholder="Tanggal Awal" autocomplete="off" />
          </div>
          <div class="col-md-4">
           <input type="text" name="end_date" id="end_date" class="form-control" placeholder="Tanggal Akhir" autocomplete="off" />
          </div>
         </div>
         <div class="col-md-4 hapus">
          <input type="button" name="search" id="search" value="Search" class="btn btn-info" />
         </div>
        </div>
        <br />
        <table id="data_balita" class="table table-bordered table-striped">
         <thead>
          <tr>
          <th>No</th>
           <th>Nama Balita</th>
           <th>Jenis Kelamin</th>
           <th>Nama Ibu</th>
           <th>Pemeriksaan Ke</th>
           <th>Berat Badan</th>
           <th>Status gizi</th>
           <th>Tanggal Pemeriksaan</th>
          </tr>
         </thead>
        </table>

       </div>
          </div>
        </div>

      </div>
      <button type="submit" name="print" onclick="printDiv('printableArea')" class="btn btn-success pull-right hapus">Cetak Laporan Data Balita</button><br><br>


  </div>

</div>




<script type="text/javascript" language="javascript" >
$(document).ready(function(){

 $('.input-daterange').datepicker({
  todayBtn:'linked',
  format: "yyyy-mm-dd",
  autoclose: true
 });

 fetch_data('no');

 function fetch_data(is_date_search, start_date='', end_date='')
 {
  var dataTable = $('#data_balita').DataTable({
   "processing" : true,
   "serverSide" : true,
   'searching'   : false,
   "order" : [],
   "ajax" : {
    url:"fetch_kunjungan.php",
    type:"POST",
    data:{
     is_date_search:is_date_search, start_date:start_date, end_date:end_date
    }
   }
  });
 }

 $('#search').click(function(){
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  if(start_date != '' && end_date !='')
  {
   $('#data_balita').DataTable().destroy();
   fetch_data('yes', start_date, end_date);
  }
  else
  {
   alert("Both Date is Required");
  }
 });

});
function printDiv(divName) {

  $('.hapus').addClass('hide');

   window.print();

   location.reload();
   return false;
}
</script>
