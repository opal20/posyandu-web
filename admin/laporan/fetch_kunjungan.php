<?php
//fetch.php
require_once '../../_assets/_fungsiTanggal.php';
require_once '../../_assets/_fungsiJam.php';
$connect = mysqli_connect("localhost", "root", "", "db_posyandu");
$columns = array('nama_balita','jenis_kelamin',  'nama_ibu', 'nomor_urut', 'berat_badan', 'status_gizi' , 'tgl_pemeriksaaan');

$query = "select nama_balita, jenis_kelamin, nama_ibu, nomor_urut ,berat_badan, status_gizi,
tgl_update AS tgl_pemeriksaaan FROM balita AS bayi
 INNER JOIN detail_balita AS detail ON detail.id_balita = bayi.id_balita
 INNER JOIN orang_tua AS ortu ON ortu.nomor_kk = bayi.nomor_kk

 ";
// $hasil = mysqli_query($connect,$query);
// while($row = mysqli_fetch_array($hasil))
//  {
//    var_dump($row);
//  }
if($_POST["is_date_search"] == "yes")
{
 $query .= 'where tgl_update BETWEEN "'.$_POST["start_date"].'" AND "'.$_POST["end_date"].'" AND ';
}
// if($_POST["is_date_search"] == "no")
// {
//
//   $output = null;
//
//   echo json_encode($output);
// }
// if(isset($_POST["search"]["value"]))
// {
//  $query .= '
//   (id_balita LIKE "%'.$_POST["search"]["value"].'%"
//   OR nama_balita LIKE "%'.$_POST["search"]["value"].'%"
//   OR nama_ibu LIKE "%'.$_POST["search"]["value"].'%"
//   OR tgl_lahir LIKE "%'.$_POST["search"]["value"].'%"
//   OR berat_lahir LIKE "%'.$_POST["search"]["value"].'%")
//  ';
// }
//
// if(isset($_POST["order"]))
// {
//  $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].'
//  ';
// }
// else
// {
//  $query .= 'ORDER BY nama_balita DESC ';
// }

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($connect, $query . $query1));

$result = mysqli_query($connect, $query . $query1);

$data = array();
$jenis_kelamin = "";
$no = 1;
while($row = mysqli_fetch_array($result))
{
  // var_dump($row);
  // exit;
 $sub_array = array();
if($row["jenis_kelamin"]=="L"){
  $jenis_kelamin="Laki - Laki";

}else{
  $jenis_kelamin="Perempuan";
}
$gizi = "Gizi Lebih";

if($row["status_gizi"] == 2){
  $gizi = "Gizi Baik";

}elseif($row["status_gizi"] == 3){
  $gizi = "Gizi Kurang";

}elseif($row["status_gizi"] == 4){
  $gizi = "Gizi Buruk";

}
$sub_array[] = $no++;
 $sub_array[] = $row["nama_balita"];
 $sub_array[] = $jenis_kelamin;
 $sub_array[] = $row["nama_ibu"];
 $sub_array[] = $row["nomor_urut"];
 $sub_array[] = $row["berat_badan"];
 $sub_array[] = $gizi;
$sub_array[] =  $row["tgl_pemeriksaaan"];
 $data[] = $sub_array;
}

//

function get_all_data($connect)
{
 $query = "SELECT * FROM balita as bayi INNER JOIN orang_tua as ortu ON ortu.nomor_kk = bayi.nomor_kk";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($connect),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>
