<html>
 <head>
  <title>Laporan Data Kunjungan</title>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <style>
   body
   {
    margin:0;
    padding:0;
    background-color:#f1f1f1;

   }
   table{
     font-size: 14px !important;
   }
   .box
   {
    width:1270px;
    padding:20px;
    background-color:#fff;
    border:1px solid #ccc;
    border-radius:5px;
    margin-top:25px;
   }
   .table-responsive{
     overflow-x: none !important
     ;
   }
  </style>


 </head>
 <?php include_once('../../header.php'); ?>

  <div class="content-wrapper">
    <div class="container">

    <section class="content">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Data Semua Balita</h3>

        </div> <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
           <div class="input-daterange">
            <div class="col-md-4">
             <input type="text" name="start_date" id="start_date" class="form-control" placeholder="Tanggal Awal" autocomplete="off" />
            </div>
            <div class="col-md-4">
             <input type="text" name="end_date" id="end_date" class="form-control" placeholder="Tanggal Akhir" autocomplete="off" />
            </div>
           </div>
           <div class="col-md-4">
            <input type="button" name="search" id="search" value="Search" class="btn btn-info" />
           </div>
          </div>
          <br />
          <table id="order_data" class="table table-bordered table-striped dataTable no-footer">
           <thead>
            <tr>
             <th>Nama Ibu</th>
             <th>Nama Balita</th>
             <th>Jenis KelaminS</th>
             <th>Tanggal Lahir</th>
             <th>Berat Lahir</th>
            </tr>
           </thead>
          </table>
        </div>



      </div>

      </section> <!-- /.content -->
    </div>

  </div>




<script type="text/javascript" language="javascript" >
$(document).ready(function(){

 $('.input-daterange').datepicker({
  todayBtn:'linked',
  format: "yyyy-mm-dd",
  autoclose: true
 });

 fetch_data('no');

 function fetch_data(is_date_search, start_date='', end_date='')
 {
  var dataTable = $('#order_data').DataTable({
   "processing" : true,
   "serverSide" : true,
   'searching'   : false,
   "order" : [],
   "ajax" : {
    url:"fetch.php",
    type:"POST",
    data:{
     is_date_search:is_date_search, start_date:start_date, end_date:end_date
    }
   }
  });
 }

 $('#search').click(function(){
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  if(start_date != '' && end_date !='')
  {
   $('#order_data').DataTable().destroy();
   fetch_data('yes', start_date, end_date);
  }
  else
  {
   alert("Both Date is Required");
  }
 });

});
</script>
