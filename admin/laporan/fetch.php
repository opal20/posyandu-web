<?php
//fetch.php
require_once '../../_assets/_fungsiTanggal.php';
require_once '../../_assets/_fungsiJam.php';
$connect = mysqli_connect("localhost", "root", "", "db_posyandu");
$columns = array('nama_balita','jenis_kelamin',  'nama_ibu', 'tgl_lahir', 'berat_lahir' , 'tgl_dibuat',);

$query = "select * from balita as balita
          inner join orang_tua on balita.nomor_kk=orang_tua.nomor_kk
         WHERE ";

if($_POST["is_date_search"] == "yes")
{
 $query .= 'balita.tgl_lahir BETWEEN "'.$_POST["start_date"].'" AND "'.$_POST["end_date"].'" AND ';
}

if(isset($_POST["search"]["value"]))
{
 $query .= '
  (id_balita LIKE "%'.$_POST["search"]["value"].'%"
  OR nama_balita LIKE "%'.$_POST["search"]["value"].'%"
  OR nama_ibu LIKE "%'.$_POST["search"]["value"].'%"
  OR tgl_lahir LIKE "%'.$_POST["search"]["value"].'%"
  OR berat_lahir LIKE "%'.$_POST["search"]["value"].'%")
 ';
}

if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].'
 ';
}
else
{
 $query .= 'ORDER BY balita.id_balita DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($connect, $query . $query1));

$result = mysqli_query($connect, $query . $query1);

$data = array();
$jenis_kelamin = "";
$no = 1;
while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
if($row["jenis_kelamin"]=="L"){
  $jenis_kelamin="Laki - Laki";

}else{
  $jenis_kelamin="Perempuan";
}
$sub_array[] = $no++;
 $sub_array[] = $row["nama_balita"];
 $sub_array[] = $row["nama_ibu"];
 $sub_array[] = $jenis_kelamin;
 $sub_array[] = tgl_indo($row["tgl_lahir"]);
 $sub_array[] = $row["berat_lahir"] . " Kg";
$sub_array[] = tgl_indo_jam($row["tgl_dibuat"]);
 $data[] = $sub_array;
}

//
// var_dump($data);
// exit;
function get_all_data($connect)
{
 $query = "SELECT * FROM balita as bayi INNER JOIN orang_tua as ortu ON ortu.nomor_kk = bayi.nomor_kk";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($connect),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>
