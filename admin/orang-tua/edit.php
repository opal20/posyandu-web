<?php
  require_once '../../koneksi.php';

  if(isset($_POST['update'])){
    $nomor = $_POST['nomor_kk'];
    $ibu   = $_POST['nama_ibu'];
    $ayah  = $_POST['nama_ayah'];
    $conn  = koneksi();

    $hasil  = mysqli_query($conn, "update orang_tua set nama_ibu='$ibu', nama_ayah='$ayah' where nomor_kk='$nomor'");
    if(!$hasil) {
      echo "<script>alert('Gagal Ubah Data')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
    else {
      echo "<script>alert('Data Berhasil Diubah')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
  }
?>

<!DOCTYPE html>
<html>
<?php include_once('../../header.php'); ?>

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">
    
    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Orang Tua
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Orang Tua</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-8">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Data Orang Tua</h3>
                </div> <!-- /.box-header -->

                <div class="box-body">
                  <!-- form start -->
                  <?php
                    $id    = $_GET['nomor_kk'];
                    $conn  = koneksi();
                    $hasil = mysqli_query($conn,"select * from orang_tua where nomor_kk='$id'");
                    $data  = mysqli_fetch_array($hasil);
                  ?>
                  <form class="form-horizontal" method="POST">
                    <input type="hidden" name="nomor_kk" value="<?=$data['nomor_kk']?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Nomor KK</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="nomor_kk" placeholder="Nomor Kartu Keluarga" value="<?=$data['nomor_kk']?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Nama Ibu</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Ibu" value="<?=$data['nama_ibu']?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Nama Ayah</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="nama_ayah" placeholder="Nama Ayah" value="<?=$data['nama_ayah']?>" required>
                        </div>
                      </div>
                    </div> <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" name="update" class="btn btn-warning pull-right"><i class="fa fa-save"></i> Simpan</button>
                    </div> <!-- /.box-footer -->
                  </form>
                </div> <!-- /.box-body -->
              </div> <!-- /.box -->
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->

        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="container">
        <div class="pull-right hidden-xs">
          version 1.0.0 | Developed by <a href="" title="Developer">Abdul Hafizh</a>
        </div>
        <strong>Copyright &copy; 2018 <a href="https://adminlte.io" target="_blank">Almsaeed Studio</a>.</strong>
      </div> <!-- /.container -->
    </footer>
  </div> <!-- ./wrapper -->

  <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../_assets/dist/js/adminlte.min.js"></script>
</body>
</html>
