<?php
  require_once '../../koneksi.php';

?>

<!DOCTYPE html>
<html>
<?php include_once('../../header.php'); ?>
    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Orang Tua
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Orang Tua</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-4">
            <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambah Orang Tua</h3>
                </div> <!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST" action="simpan.php">
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nomor KK</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="nomor_kk" placeholder="Nomor Kartu Keluarga" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nama Ibu</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Ibu" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Nama Ayah</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="nama_ayah" placeholder="Nama Ayah" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="email" placeholder="Email" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">No Telp</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="notlp" placeholder="No Telp" required>
                      </div>
                    </div>
                  </div> <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="reset" class="btn btn-default">Batal</button>
                    <button type="submit" name="simpan" class="btn btn-success pull-right">Simpan</button>
                  </div> <!-- /.box-footer -->
                </form>
              </div> <!-- /.box -->
            </div> <!--/.col-4 -->

            <div class="col-md-8">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Orang Tua</h3>
                </div> <!-- /.box-header -->

                <div class="box-body">
                  <table id="posyandu" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nomor KK</th>
                        <th>Nama Ibu</th>
                        <th>Nama Ayah</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $conn = koneksi();
                        //$count = "select count(id_user) as jumlah_user from user";
                        //$jumlah = mysqli_query($conn, $count);
                        //$jumlah_user = mysqli_fetch_array($jumlah);
                        //echo $jumlah['jumlah_user'];
                        $sql =  "select * from orang_tua";
                        //$strings = "";

                          //  $cek_kk = false;


                        $hasil = mysqli_query($conn, $sql);
                        $no    = 1;

                        while ($data = mysqli_fetch_array($hasil)) {
                      ?>
                      <tr>
                        <td><?=$no++?></td>
                        <td><?=$data["nomor_kk"]?></td>
                        <td><?=$data["nama_ibu"]?></td>
                        <td><?=$data["nama_ayah"]?></td>
                        <td>
                          <a href="detail.php?nomor_kk=<?=$data['nomor_kk'];?>" class="btn btn-sm btn-primary" title="Detail Data"><i class="fa fa-search"></i></a>
                          <a href="edit.php?nomor_kk=<?=$data['nomor_kk'];?>" class="btn btn-sm btn-warning" title="Edit Data"><i class="fa fa-pencil"></i></a>
                          <a href="hapus.php?nomor_kk=<?=$data['nomor_kk'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')" title="Hapus Data"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                      <?php } ?>

                    </tbody>
                  </table>
                </div> <!-- /.box-body -->

              </div> <!-- /.box -->
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->

        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="container">
        <div class="pull-right hidden-xs">
          version 1.0.0 | Developed by <a href="" title="Developer">Abdul Hafizh</a>
        </div>
        <strong>Copyright &copy; 2018 <a href="https://adminlte.io" target="_blank">Almsaeed Studio</a>.</strong>
      </div> <!-- /.container -->
    </footer>
  </div> <!-- ./wrapper -->

  <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../_assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../../_assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="../../_assets/dist/js/adminlte.min.js"></script>
  <script>
    $(function () {
      $('#posyandu').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'info'        : true,
        'autoWidth'   : true,
        'columnDefs': [
          {
            "targets": [ 4 ],
            "orderable": false,
            "searchable": false
          },
          {
            "targets": [ 0 ],
            "searchable": false
          }
        ]
      })
    })
  </script>
</body>
</html>
