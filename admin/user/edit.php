<?php
  require_once '../../koneksi.php';


  if(isset($_POST['update'])){
    $id    = $_GET['id_user'];
    $nama   = $_POST["username"];

    $level = $_POST['level'];
    $conn   = koneksi();


    if($_POST['password'] != '')
		{
      $password = password_hash($_POST["password"], PASSWORD_DEFAULT);
      $hasil  = mysqli_query($conn, "update
                                    user set
                                    username='$nama',
                                    password='$password',
                                    level='$level'
                                    where id_user='$id'");

		}
		else
		{
			$hasil  = mysqli_query($conn, "update
                                     user set
                                     username='$nama',
                                     level='$level' where id_user='$id'");
		}
    if(!$hasil) {
      echo "<script>alert('Gagal Ubah Data')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
    else {
      echo "<script>alert('Data Berhasil Diubah')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
  }
?>

<!DOCTYPE html>
<html>
<?php include_once('../../header.php'); ?>
    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            User
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Posyandu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-8">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Data User</h3>
                </div> <!-- /.box-header -->

                <div class="box-body">
                  <!-- form start -->
                  <?php
                    $id    = $_GET['id_user'];
                    $conn  = koneksi();
                    $hasil = mysqli_query($conn,"select * from user where id_user='$id'");
                    $data  = mysqli_fetch_array($hasil);
                  ?>
                  <form class="form-horizontal" method="POST">
                    <input type="hidden" name="id" value="<?=$data['id']?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Username</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="username" placeholder="Nama Posyandu" value="<?=$data['username']?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Password</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="password" placeholder="password"  >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Pilih Level</label>
                        <div class="col-sm-7">
                          <select class="form-control select2" name="level" >
                              <option value="" readonly >Pilih</option>
                            <?php
                            $output = '';
                            $array = [
                              '0'=>Admin,
                              '1'=>Kader,
                               '2'=>Bidan,
                               '3'=>Orang_Tua];


                            $pilih= $data['level'];



                              // code...

                              ?>
                              <?php foreach ($array as $key => $value): ?>
                                <option value="<?php echo $key;?>"<?php echo ($pilih == $key) ? "selected": "" ?>>
                                  <?php   echo  $value;?></option>
                              <?php endforeach; ?>


                            <?php


                             ?>



                          </select>
                          <?php

                         ?>
                        </div>
                      </div>
                    </div> <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" name="update" class="btn btn-warning pull-right"><i class="fa fa-save"></i> Simpan</button>
                    </div> <!-- /.box-footer -->
                  </form>
                </div> <!-- /.box-body -->
              </div> <!-- /.box -->
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->

        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="container">
        <div class="pull-right hidden-xs">
          version 1.0.0 | Developed by <a href="" title="Developer">Abdul Hafizh</a>
        </div>
        <strong>Copyright &copy; 2018 <a href="https://adminlte.io" target="_blank">Almsaeed Studio</a>.</strong>
      </div> <!-- /.container -->
    </footer>
  </div> <!-- ./wrapper -->

  <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../_assets/dist/js/adminlte.min.js"></script>
</body>
</html>
