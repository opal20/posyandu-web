<?php
  require_once '../../koneksi.php';



  if(isset($_POST["simpan"])){
    $conn  = koneksi();
     $username   = $_POST['username'];
     $email = $_POST['email'];
     $no_tlp = $_POST['notlp'];
    $password = password_hash($_POST["password"], PASSWORD_DEFAULT);
    $level = $_POST['level'];
    $ket = "";
    if($level == 0 ){
      $ket = "admin";
    }else if ($level == 1){
      $ket = "kader";
    }else if ($level == 2){
      $ket = "bidan";
    }else if ($level == 3){
      $ket = "ortu" ;
    }
    //select count dulu untuk menjumlah admin sesuai level
    $query = "select count(level) as count from user where level = '$level'";
    $fect_data_count = mysqli_query($conn, $query);
    $data_count = mysqli_fetch_array($fect_data_count);
    $count = $data_count['count'] + 1;
    // var_dump($count);
    // exit;
    $input_username = $ket.$count."-".$username;

    $sql   = "insert into user (username,password,level,email,no_tlp) values('$input_username','$password', '$level' ,'$email','$no_tlp')";
    $hasil = mysqli_query($conn, $sql);
    if(!$hasil) {
      echo "<script>alert('Gagal Simpan')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
    else {
      echo "<script>alert('Data Berhasil Disimpan')</script>";
      echo "<html><head><meta http-equiv='refresh' content='0;url=./'></head><body></body></html>";
    }
  }
?>

<!DOCTYPE html>
<html>
<?php include_once('../../header.php');

 $cek =  $_SESSION['level'];
 $admin = false;
 $kader = false;
 $bidan = false;
 $ortu = false;
 if ($cek == 0){
     $admin = true;
 }elseif ($cek == 1){

   $kader = true;
 }elseif ($cek == 2) {
   // code...

   $bidan = true;

}elseif ($cek == 3) {
 // code...

 $ortu = true;
}
?>

    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
            Posyandu
            <small>Kelola Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-dashboard"></i> <?php echo $_SESSION['username'] ?></a></li>
            <li class="active">Posyandu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-4">
            <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambah User</h3>
                </div> <!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="email" placeholder="Email" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Username</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="username" placeholder="Nama User" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Password</label>
                      <div class="col-sm-7">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">No Telp</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="notlp" placeholder="No Telp" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Pilih Level</label>
                      <div class="col-sm-7">
                        <select class="form-control select2" name="level" required>
                          <option value="" readonly >Pilih</option>

                          <option value="1">Kader</option>
                          <option value="2">Bidan</option>
                          

                        </select>
                      </div>
                    </div>

                  </div> <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="reset" class="btn btn-default">Batal</button>
                    <button type="submit" name="simpan" class="btn btn-success pull-right">Simpan</button>
                  </div> <!-- /.box-footer -->
                </form>
              </div> <!-- /.box -->
            </div> <!--/.col-4 -->

            <div class="col-md-8">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Data User</h3>
                </div> <!-- /.box-header -->

                <div class="box-body">
                  <table id="user" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama User</th>
                        <th>Level</th>
                        <?php if($kader){ ?>
                        <th>Action</th>
                          <?php }   ?>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $conn = koneksi();
                        $sql =  "select * from user";

                        $hasil = mysqli_query($conn, $sql);
                        $no    = 1;
                        while ($data = mysqli_fetch_array($hasil)) {
                          $array = [
                            'Admin',
                            'Kader',
                             'Bidan',
                             'Orang_Tua'];
                          $pilih= $data['level'];
                          $me = '';
                          if ($data["username"] == $_SESSION['username']){
                              $me = ' [Saya]';
                          }

                      ?>
                      <tr>
                        <td><?=$no++?></td>
                        <td><?=$data["username"].$me?></td>
                        <td><?php
                          echo print_r($array[$pilih], true);

                        ?></td>
                        <td>
                          <?php if($kader){ ?>
                          <a href="edit.php?id_user=<?=$data['id_user'];?>" class="btn btn-sm btn-warning" title="Edit Data"><i class="fa fa-pencil"></i></a>
                          <a href="hapus.php?id_user=<?=$data['id_user'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')" title="Hapus Data"><i class="fa fa-trash"></i></a>
                          <?php }   ?>
                        </td>
                      </tr>
                      <?php }   ?>

                    </tbody>
                  </table>
                </div> <!-- /.box-body -->

              </div> <!-- /.box -->
            </div> <!--/.col-8 -->
          </div> <!-- /.row -->

        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

    <?php include_once('../footer.php'); ?>
  </div> <!-- ./wrapper -->

  <script src="../../_assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../_assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../../_assets/bower_components/select2/dist/js/select2.full.min.js"></script>
  <script src="../../_assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="../../_assets/dist/js/adminlte.min.js"></script>
  <script>
    $(function () {
      $('#user').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'info'        : true,
        'autoWidth'   : true,
        'columnDefs': [
          {
            "targets": [ 2,3 ],
            "orderable": false,
            "searchable": false
          },
          {
            "targets": [ 0 ],
            "searchable": false
          }
        ]
      })
        $('.select2').select2()
    })
  </script>
</body>
</html>
