<?php include('header.php');
require_once '../_assets/_fungsiTanggal.php';
?>
<?php $date_now = date("Y-m-d");

 ?>
    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <section class="content-header">
          <h1>
          Laporan Kunjungan Per tanggal <?= tgl_indo($date_now) ?>

          </h1>
          <ol class="breadcrumb">
            <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Selamat Datang </li> <?php echo $_SESSION['username'] ?>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div id="printableArea">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Data Kunjungan Per tanggal <?= tgl_indo($date_now) ?></h3>

              </div> <!-- /.box-header -->

              <div class="box-body">



                <table id="posyandu" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Balita</th>
                      <th>Nama Ibu</th>
                      <th>Jenis Kelamin</th>
                      <th>Tanggal Lahir</th>
                      <th>Pemeriksaan Ke</th>
                      <th>Status Gizi</th>
                      <!-- <th>Action</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $conn = koneksi();
                      $sql =  "select * FROM balita AS bayi INNER JOIN detail_balita AS detail ON detail.id_balita = bayi.id_balita INNER JOIN orang_tua AS ortu ON ortu.nomor_kk = bayi.nomor_kk WHERE detail.tgl_update = '$date_now'";

                      $hasil = mysqli_query($conn, $sql);

                      $no    = 1;
                      while ($data = mysqli_fetch_array($hasil)) {
                        // var_dump($data);
                        $gender="";
                        if($data["jenis_kelamin"]=="L"){
                            $gender="Laki-laki";
                        }
                        if($data["jenis_kelamin"]=="P"){
                            $gender="Perempuan";
                        }

                        $gizi = "Gizi Lebih";
                        $warna = "blue";
                        if($data["status_gizi"] == 2){
                          $gizi = "Gizi Baik";
                          $warna = "green";
                        }elseif($data["status_gizi"] == 3){
                          $gizi = "Gizi Kurang";
                          $warna = "yellow";
                        }elseif($data["status_gizi"] == 4){
                          $gizi = "Gizi Buruk";
                          $warna = "red";
                        }
                    ?>
                    <tr>
                      <td><?=$no++?></td>
                      <td><?=$data["nama_balita"]?></td>
                      <td><?=$data["nama_ibu"]?></td>
                      <td><?=$gender?></td>
                      <td><?=tgl_indo($data["tgl_lahir"])?></td>
                      <td><?=$data["nomor_urut"]?></td>
                      <?php if($warna=="yellow"){
                        ?>
                        <td style="background:<?=$warna?>;color:red"><?=$gizi?></td>
                    <?php  } else { ?>
                      <td style="background:<?=$warna?>;color:white"><?=$gizi?></td>
                    <?php } ?>
                      <!-- <td>

                      <a href="detail.php?id=<?=$data['id_balita'];?>" class="btn btn-sm btn-primary" title="Detail Data"><i class="fa fa-search"></i></a>

                      </td> -->
                    </tr>
                    <?php } ?>

                  </tbody>
                </table>
              </div>

                </div> <!-- /.box-body -->

            </div> <!-- /.box -->
              <button type="submit" name="print" onclick="printDiv('printableArea')" class="btn btn-success pull-right">Cetak Laporan</button><br><br>
          </div>
        </section> <!-- /.content -->
      </div> <!-- /.container -->
    </div> <!-- /.content-wrapper -->

<?php include_once('footer.php'); ?>
<script>
  $(function () {

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight:true
    })
    $('.select2').select2()
    $('#posyandu').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'info'        : true,
      'autoWidth'   : true,
      'columnDefs': [

        {
          "targets": [ 5 ],
          "searchable": false,
          "orderable": false
        },
        {
          "targets": [ 0 ],
          "searchable": false
        },
      ]
    })
  })
  function printDiv(divName) {

     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
     location.reload();
     return false;
  }
</script>
